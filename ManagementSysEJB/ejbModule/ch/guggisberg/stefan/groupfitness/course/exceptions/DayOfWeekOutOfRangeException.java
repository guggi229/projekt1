package ch.guggisberg.stefan.groupfitness.course.exceptions;

public class DayOfWeekOutOfRangeException extends Exception {
	private static final long serialVersionUID = 3078698317317421285L;

	public DayOfWeekOutOfRangeException() {
		super("DayOfWeek muss zwischen 1 und 7 liegen.");
	}
}


