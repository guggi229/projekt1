package ch.guggisberg.stefan.propertiesExporter;




import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;



public class PropertiesExporter {

	private static final String CONFIG_FILE = "app.properties";
	private static final PropertiesConfiguration CONFIG;
	private static final PropertiesExporter INSTANCE = new PropertiesExporter();
	
	

	// Properties
	private static final String PROPERTY_REGEX_NAME= "regex.text";
	
	static {
		CONFIG = new PropertiesConfiguration();
		CONFIG.setListDelimiter('|');
		CONFIG.setFileName(CONFIG_FILE);
		CONFIG.setReloadingStrategy(new FileChangedReloadingStrategy());
		try {
			CONFIG.load();
		} catch (ConfigurationException ce) {
			throw new ExceptionInInitializerError(ce);
		}
	}
	
	private PropertiesExporter() {
		// sollte nicht instanziert werden
	}

	public static PropertiesExporter getInstance() {
		return INSTANCE;
	}
	
	public static String getRegexText() {
		return CONFIG.getString(PROPERTY_REGEX_NAME);
	}
	

}
