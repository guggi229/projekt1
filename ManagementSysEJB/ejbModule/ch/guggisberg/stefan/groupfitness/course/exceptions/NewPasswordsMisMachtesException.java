package ch.guggisberg.stefan.groupfitness.course.exceptions;

/**
 * Wird geworfen, wenn das neue Passwort nicht mit dem neuen wiederholte Password übereinstimmt.
 * @author guggi229
 *
 */
public class NewPasswordsMisMachtesException extends Exception {

	private static final long serialVersionUID = 1669530412726738883L;

	public NewPasswordsMisMachtesException(String errorMessage) {
		super(errorMessage);
	}
}
