package ch.guggisberg.stefan.usermanagement.model.requests;

import java.io.Serializable;

public class RequestKursDeleteTrxModel implements Serializable {

	private static final long serialVersionUID = -3226215635567628515L;
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
