package ch.guggisberg.stefan.groupfitness.course.services;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;

import ch.guggisberg.stefan.groupfitness.course.entities.Room;

@Stateless
@LocalBean
@DeclareRoles({"GrpAdmin", "customer", "Admin", "instruktor","admin"})
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class RoomService extends BaseCrud<Room> {
	private static Logger log = Logger.getLogger(RoomService.class);

	@RolesAllowed("GrpAdmin")
	public List<Room> getAllRooms(){
		return findByNamedQuery(Room.QUERY_FIND_ALL_ROOMS);
	}

	@RolesAllowed("GrpAdmin")
	public Room getRoom (Long id) {
		return entityManager.find(Room.class, id);
	}
	@RolesAllowed("GrpAdmin")
	public void remove (Long id) {
		Room r = getRoom(id);
		r.setDeleted(true);
		entityManager.merge(r);
		log.info(MessageFormat.format("Raum: {0} gelöscht", r.getNameDe()));	
	}
	@RolesAllowed("GrpAdmin")
	@Override
	public Room find(Class<Room> type, Object id) {
		return super.find(type, id);
	}
	
	@RolesAllowed("GrpAdmin")
	public void createKurs(Room r) {
		entityManager.persist(r);
	}



}
