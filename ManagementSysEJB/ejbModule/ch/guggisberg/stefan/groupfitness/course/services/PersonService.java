package ch.guggisberg.stefan.groupfitness.course.services;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.annotation.security.DeclareRoles;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;

import ch.guggisberg.stefan.groupfitness.course.entities.Person;
import ch.guggisberg.stefan.groupfitness.course.entities.PersonAuthenticationTrx;
import ch.guggisberg.stefan.groupfitness.course.exceptions.CrudServiceException;
import ch.guggisberg.stefan.groupfitness.course.exceptions.NewPasswordsMisMachtesException;
import ch.guggisberg.stefan.groupfitness.course.exceptions.PasswordWrongException;

@Stateless
@LocalBean
@DeclareRoles({"GrpAdmin", "customer", "Admin", "instruktor","admin"})
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class PersonService extends BaseCrud<Person> {
	private static Logger log = Logger.getLogger(PersonService.class);

	@Resource 
	private SessionContext ctx;
	private String userName;

	@RolesAllowed("GrpAdmin")
	public Person createPerson(Person p) {
		entityManager.persist(p);
		return p;
	}

	@RolesAllowed("GrpAdmin")
	public Person updatePerson(Person p) {
		return entityManager.merge(p);
	}

	@RolesAllowed("GrpAdmin")
	@SuppressWarnings("unchecked")
	public List<Person> getAllPersons(){
		return entityManager.createNamedQuery(Person.QUERY_FIND_ALL_PERSON).getResultList();
	}

	@RolesAllowed("GrpAdmin")
	public List<Person> getFilteredPersons(String filter){
		filter= (filter.equals("") ? "%%" : MessageFormat.format("%{0}%", filter));
		filter=filter.toUpperCase();
		log.info(MessageFormat.format("Filter: {0}", filter));
		HashMap<String, Object> params = new HashMap<>();
		params.put(Person.PARAM_FILTER, filter);
		return findByNamedQuery(Person.QUERY_FIND_FILTERED_PERSON, params);
	}

	@RolesAllowed("GrpAdmin")
	public List<Person> getFilteredPersons2(String filter, Integer startPosition, Integer maxRows){
		filter= (filter.equals("") ? "%%" : MessageFormat.format("%{0}%", filter));
		filter=filter.toUpperCase();
		log.info(MessageFormat.format("Filter: {0}", filter));
		HashMap<String, Object> params = new HashMap<>();
		params.put(Person.PARAM_FILTER, filter);
		return findByNamedQuery(Person.QUERY_FIND_FILTERED_PERSON, params, startPosition, maxRows);
	}

	@RolesAllowed({"GrpAdmin", "customer", "Admin", "instruktor","admin"})
	public Person changePassword(String newPassword,String newPasswordRepeated, String oldPassword) throws NewPasswordsMisMachtesException,  PasswordWrongException {
		// Stimmen die neuen Passwörter überein?
		if(!newPassword.equals(newPasswordRepeated)) {
			throw new NewPasswordsMisMachtesException("Die neuen Passwörter stimmen nicht");
		}

		// Entspricht das Passwort den Anforderungen?

		// To do

		// Testen ob der eingeloggte User das Kennwort kennt.

		userName=(ctx.getCallerPrincipal().getName());
		HashMap<String, Object> params = new HashMap<>();
		params.put(Person.PARAM_EMAIL, userName);
		params.put(Person.PARAM_PASSWORD, oldPassword);
		try {
			Person p = findSingleResultNamedQuery(Person.QUERY_FIND_PERSON_BY_EMAIL_AND_PASSWORD,params);
			p.setPassword(newPassword);
			return entityManager.merge(p);
		} catch (Exception e) {
			throw new PasswordWrongException( MessageFormat.format("User {0} gab falsches Passwort ein. ", ctx.getCallerPrincipal().getName())) ;}
	}

	@RolesAllowed("GrpAdmin")
	public Person getPerson (Long id) {
		return entityManager.find(Person.class, id);
	}
	
	@RolesAllowed("GrpAdmin")
	public void remove (Long id) {
		Person p = getPerson(id);
		p.setDeleted(true);
		entityManager.merge(p);
		log.info(MessageFormat.format("Person: {0} gelöscht", p.getEmail()));	
	}
	
	/**
	 * Liefert den eingeloggten User zurück.
	 * @return Person
	 */
	@RolesAllowed({"GrpAdmin", "customer", "Admin", "instruktor","admin"})
	public  Person getLoggedPerson() {
		HashMap<String, Object> params = new HashMap<>();
		params.put(Person.PARAM_EMAIL, ctx.getCallerPrincipal().getName());
		return findSingleResultNamedQuery(Person.QUERY_FIND_PERSON_BY_EMAIL,params);
	}
	
	@PermitAll
	public boolean checkUserPrincipals(Person p) throws CrudServiceException {
		HashMap<String, Object> params = new HashMap<>();
		params.put(Person.PARAM_EMAIL, p.getEmail());
		params.put(Person.PARAM_PASSWORD, p.getPassword());
		return ((executeNamedQuery(Person.QUERY_CHECK_PERSON_PRINCIPALS, params) >0 ) ? true:false);
	}
	
	/**
	 * "Super" Methode erweitern mit den Security Contraints
	 */

	@RolesAllowed("GrpAdmin")
	@Override
	public Person find(Class<Person> type, Object id) {
		return super.find(type, id);
	}
	@RolesAllowed("GrpAdmin")
	@Override
	public Person update(Person p) {
		return super.update(p);
	}
	
}



