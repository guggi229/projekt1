package ch.guggisberg.stefan.classController;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import ch.guggisberg.stefan.base.BaseBean;
import ch.guggisberg.stefan.groupfitness.course.entities.Kurs;
import ch.guggisberg.stefan.groupfitness.course.services.ClassService;

@Named
@SessionScoped
public class ClassController extends BaseBean implements Serializable {
	private static final long serialVersionUID = 3263438832673772158L;

	@EJB
	private ClassService cs;

	private Kurs kurs = new Kurs();

	//NAV
	
	private static final String NAVIGATE_TO_CLASSES_LIST = "/grpAdmin/classViews/classesList";

	public List<Kurs> getFilteredClasses(){
		return cs.getFilteredClasses(filter);
	}

	public String search() {
		return NAVIGATE_TO_CLASSES_LIST;
	}

	public String editView(Kurs k) {
		kurs = cs.find(Kurs.class, k.getId());
		return NAVIGATE_TO_CLASSES_LIST;
	}

	public String update() {
		kurs= cs.updateKurs(kurs);
		showGlobalMessage("messages.class.changes");
		return NAVIGATE_TO_CLASSES_LIST;
	}

	
	public String deleteView(Kurs k) {
		cs.remove(k.getId());
		showGlobalMessage("messages.class.deleted");
		return NAVIGATE_TO_CLASSES_LIST;
	}

	public String createKurs() {
		showGlobalMessage("messages.class.created");
		kurs = cs.createKurs(kurs);
		
		return NAVIGATE_TO_CLASSES_LIST;
	}
	// Getter / Setter

	public Kurs getKurs() {
		return kurs;
	}

	public void setKurs(Kurs kurs) {
		this.kurs = kurs;
	}
	public Kurs getKursById(Long id) {
		return cs.getKursById(id);
	}

	



}
