package ch.guggisberg.stefan.classAddStatisticsController;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.apache.log4j.Logger;

import ch.guggisberg.stefan.base.BaseBean;
import ch.guggisberg.stefan.groupfitness.course.entities.ClassRun;
import ch.guggisberg.stefan.groupfitness.course.exceptions.WrongPersonException;
import ch.guggisberg.stefan.groupfitness.course.services.ClassRunService;
import ch.guggisberg.stefan.groupfitness.course.services.MonthlyPayrollService;
import ch.guggisberg.stefan.groupfitness.course.services.PersonService;
import ch.guggisberg.stefan.utils.monthlyJear.MonthlyYearFilter;

@Named
@SessionScoped
public class ClassAddStatisticsController extends BaseBean implements MonthlyYearFilter {

	private static final long serialVersionUID = -613855777758691745L;
	private static Logger log = Logger.getLogger(ClassAddStatisticsController.class);
	private static final Month[] MONTH = Month.values();

	@EJB
	private ClassRunService crs;
	
	@EJB
	private PersonService ps;

	@EJB
	private MonthlyPayrollService mp; 
	
	private ClassRun classRun;
	private String stringSelectedYear = Integer.toString(LocalDate.now().getYear());
	private int selectedMonth = LocalDate.now().getMonthValue();
	private boolean classBooked;
	
	// Nav
	private final static String NAVIGATE_TO_OVERVIEW_AMOUNT_OF_CUSTOMER = "/instructor/overviewAmountOfCustomer"; 
	private final static String NAVIGATE_TO_CLASS_ADD_STATISTICS = "/instructor/classAddStatistics";
	private final static String NAVIGATE_TO_MONTHLY_GRADUATION= "/instructor/monthlyGraduation";
	
	public List<ClassRun> getMyClassRuns(){
		
		return	crs.getMyClassRunsInDateRange(ps.getLoggedPerson().getId(), getFirstDay(stringSelectedYear, selectedMonth), getLastDay(stringSelectedYear, selectedMonth));
	}
	
	public List<ClassRun> getMyBookedClassRuns(){
		LocalDate selectedDate = LocalDate.of(Year.parse(stringSelectedYear).getValue(), selectedMonth, 1);
		return	crs.getMyClassRunsInDateRange(ps.getLoggedPerson().getId(),  getFirstDay(stringSelectedYear, selectedMonth), getLastDay(stringSelectedYear, selectedMonth));
	}

	public String search() {
		return NAVIGATE_TO_OVERVIEW_AMOUNT_OF_CUSTOMER;
	}
	
	public String filter() {
		return NAVIGATE_TO_MONTHLY_GRADUATION;
	}
	public String saveAmountCustomer(ClassRun cr) {
		classRun = cr;	
		return NAVIGATE_TO_CLASS_ADD_STATISTICS;
		}

	public String save()  {
		try {
			crs.addcustomerAmount(classRun);
			showGlobalMessage("messages.statistic.customer.saved");

		} catch (WrongPersonException e) {
			// Sollte nie passieren können.
			showGlobalErrorMessage("error.messages.hopla");
			log.error("Person versucht einen fremden Kurs updaten: ", e);
		}
		classRun=null;
		return NAVIGATE_TO_OVERVIEW_AMOUNT_OF_CUSTOMER;
	}
	
	public ClassRun getClassRun() {
		return classRun;
	}

	public void setClassRun(ClassRun classRun) {
		this.classRun = classRun;
	}

	public Month[] getMonth() {
		return MONTH;
	}

	
	public int getSelectedMonth() {
		return selectedMonth;
	}

	public void setSelectedMonth(int selectedMonth) {
		this.selectedMonth = selectedMonth;
	}
	public String getStringSelectedYear() {
		return stringSelectedYear;
	}

	public void setStringSelectedYear(String stringSelectedYear) {
		this.stringSelectedYear = stringSelectedYear;
	}

	public boolean isClassBooked() {
		return classBooked;
	}

	public void setClassBooked(boolean classBooked) {
		this.classBooked = classBooked;
	}
	
	
}
