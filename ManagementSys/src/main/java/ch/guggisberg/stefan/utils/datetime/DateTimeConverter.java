package ch.guggisberg.stefan.utils.datetime;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;

/**
 * 
 * @author guggi229
 *
 * Konvetiert Java 8 DateTime in utils.date für JSF 2.x
 */

@SuppressWarnings("rawtypes")
@FacesConverter(value="localDateTimeConverter")
public class DateTimeConverter implements javax.faces.convert.Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
          return LocalDate.parse(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        LocalDate dateValue = (LocalDate) value;
        return dateValue.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }
    
}