package ch.guggisberg.stefan.groupfitness.course.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.guggisberg.stefan.groupfitness.course.model.State;

@Entity
@Table(name="CLASSRUN")
@NamedQueries({

	@NamedQuery(name= ClassRun.QUERY_FIND_ALL_CLASS_RUNS, 
			query ="Select c FROM ClassRun c"),

	@NamedQuery(name= ClassRun.QUERY_FIND_MY_CLASS_RUNS, 
	query ="SELECT c FROM ClassRun c WHERE c.person.id=:" + ClassRun.PARAM_FILTER_PERSON_ID), 

	@NamedQuery(name= ClassRun.QUERY_FIND_MY_CLASS_RUNS_IN_DATE_RANGE, 
	query ="SELECT c FROM ClassRun c WHERE c.person.id=:" + ClassRun.PARAM_FILTER_PERSON_ID  + " AND  c.date BETWEEN :"+ ClassRun.PARAM_FILTER_START_DATE + " AND :"+ClassRun.PARAM_FILTER_END_DATE  ),

	@NamedQuery(name= ClassRun.QUERY_FIND_MY_CLASS_RUNS_BY_BOOK_STATE, 
	query ="SELECT c FROM ClassRun c WHERE c.person.id=:" + ClassRun.PARAM_FILTER_PERSON_ID  + " AND  c.date BETWEEN :"+ ClassRun.PARAM_FILTER_START_DATE + " AND :"+ClassRun.PARAM_FILTER_END_DATE + " AND c.salarBooked=:" + ClassRun.PARAM_FILTER_IS_BOOKED  ),
	
	@NamedQuery(name= ClassRun.QUERY_FIND_MY_CLASS_RUNS_IN_DATE_RANGE_AND_BOOKED, 
	query ="SELECT c FROM ClassRun c WHERE c.state=:" + ClassRun.PARAM_FILTER_BOOKED_STATE + " AND c.person.id=:" + ClassRun.PARAM_FILTER_PERSON_ID + " AND c.date <=:"+ ClassRun.PARAM_FILTER_START_DATE)
})

public class ClassRun implements Serializable {

	private static final long serialVersionUID = -8818800510990342983L;

	public static final String QUERY_FIND_ALL_CLASS_RUNS="QUERY_FIND_ALL_CLASS_RUNS";
	public static final String QUERY_FIND_MY_CLASS_RUNS_BY_BOOK_STATE="QUERY_FIND_MY_CLASS_RUNS_BY_BOOK_STATE";

	public static final String QUERY_FIND_MY_CLASS_RUNS="QUERY_FIND_MY_CLASS_RUNS";
	public static final String QUERY_FIND_MY_CLASS_RUNS_IN_DATE_RANGE="QUERY_FIND_MY_CLASS_RUNS_IN_DATE_RANGE";
	public static final String QUERY_FIND_MY_CLASS_RUNS_IN_DATE_RANGE_AND_BOOKED="QUERY_FIND_MY_CLASS_RUNS_IN_DATE_RANGE_AND_BOOKED";

	public static final String PARAM_FILTER_BOOKED_STATE= "PARAM_FILTER_BOOKED_STATE";
	public static final String PARAM_FILTER_PERSON_ID= "PARAM_FILTER_PERSON_ID";
	public static final String PARAM_FILTER_START_DATE= "PARAM_FILTER_START_DATE";
	public static final String PARAM_FILTER_END_DATE= "PARAM_FILTER_END_DATE";
	public static final String PARAM_FILTER_IS_BOOKED= "PARAM_FILTER_IS_BOOKED";


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private Long id;

	@Column(name="DATE")
	
	private LocalDate date;

	@Column(name="TIME")
	private LocalTime startTime;

	@Column(name="AMOUNT_CUSTOMER")
	
	private int amountCustomer;

	@Column(name="SALARY_BOOKED")
	@JsonIgnore
	private boolean salarBooked;

	@Column(name="DURATION")
	@JsonIgnore
	private int duration;
	
	@JsonIgnore
	@Column(name="DELETED")
	private boolean deleted;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ROOMID")
	@JsonIgnore
	private Room room;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CLASSID")
	
	private Kurs kurs;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "PERSONID")
	@JsonIgnore
	private Person person;

	@Enumerated(EnumType.STRING)
	@Column(name = "STATUS")
	@JsonIgnore
	private State state;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "MonthlyPayrollID")
	@JsonIgnore
	private MonthlyPayroll monthlyPayroll;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	public int getAmountCustomer() {
		return amountCustomer;
	}

	public void setAmountCustomer(int amountCustomer) {
		this.amountCustomer = amountCustomer;
	}

	public boolean isSalarBooked() {
		return salarBooked;
	}

	public void setSalarBooked(boolean salarBooked) {
		this.salarBooked = salarBooked;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public Kurs getKurs() {
		return kurs;
	}

	public void setKurs(Kurs kurs) {
		this.kurs = kurs;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public MonthlyPayroll getMonthlyPayroll() {
		return monthlyPayroll;
	}

	public void setMonthlyPayroll(MonthlyPayroll monthlyPayroll) {
		this.monthlyPayroll = monthlyPayroll;
	}
	



}
