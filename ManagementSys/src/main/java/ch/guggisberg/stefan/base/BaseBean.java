package ch.guggisberg.stefan.base;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.Locale;
import java.util.ResourceBundle;

@SessionScoped
@Named
public abstract class BaseBean implements Serializable {
	protected String filter="";
	private static final long serialVersionUID = 2274757853806790722L;
	private static Logger log = Logger.getLogger(BaseBean.class);
	private static final String MESSAGES = "messages";
	private String lang;

	/**
	 * Gibt eine globale Info aus
	 * @param key
	 */
	public void showGlobalMessage(String key){
		FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO,getText(key), null);
		FacesContext fc = FacesContext.getCurrentInstance();
		fc.addMessage(null, facesMsg);
	}

	/**
	 * Gibt eine globale Fehlermeldung aus.
	 * @param key
	 */
	public void showGlobalErrorMessage(String key){
		FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR,getText(key), null);
		FacesContext fc = FacesContext.getCurrentInstance();
		fc.addMessage(null, facesMsg);
	}

	/**
	 * Sucht mit dem key im Messages Bundle nach der Übersetzung
	 * @param key
	 */
	public String getText(String key) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		Locale locale = facesContext.getViewRoot().getLocale();
		ResourceBundle bundle = ResourceBundle.getBundle(MESSAGES, locale);
		return bundle.getString(key);
	}


	// Getter / Setter

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}
	public String getLang() {
		return FacesContext.getCurrentInstance().getViewRoot().getLocale().getLanguage();
	}

	public void setLang(String lang) {
		this.lang = lang;
	}




}
