package ch.guggisberg.stefan.groupfitness.course.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.guggisberg.stefan.groupfitness.course.model.SupprtedLanguages;

@Entity
@Table(name="PERSON")
@NamedQueries({
	@NamedQuery(name= Person.QUERY_FIND_ALL_PERSON, 
			query ="Select p FROM Person p WHERE  p.deleted=false"),
	@NamedQuery(name= Person.QUERY_FIND_PERSON_BY_EMAIL_AND_PASSWORD, 
	query ="Select p FROM Person p where p.email=:" + Person.PARAM_EMAIL + " AND p.password=:" +Person.PARAM_PASSWORD +" AND p.deleted=false"),
	@NamedQuery(name= Person.QUERY_FIND_FILTERED_PERSON, query ="Select p FROM Person p WHERE  p.deleted=false "
			+ "AND ( upper(p.firstName) like :" + Person.PARAM_FILTER + " "
			+ "OR upper(p.name) like :" + Person.PARAM_FILTER + " "
			+ "OR upper(p.email) like :" + Person.PARAM_FILTER + ")"),
	@NamedQuery(name= Person.QUERY_FIND_PERSON_BY_EMAIL, 
	query ="Select p FROM Person p where p.email=:" + Person.PARAM_EMAIL +" AND p.deleted=false"),
	@NamedQuery(name= Person.QUERY_CHECK_PERSON_PRINCIPALS, 
	query ="SELECT COUNT(p) FROM Person p WHERE p.email=:" + Person.PARAM_EMAIL + " AND p.password=:" + Person.PARAM_PASSWORD)
})


//	query ="SELECT COUNT(p) FROM Person p WHERE p.email=:" + Person.PARAM_EMAIL +  " AND p.password=:" + Person.PARAM_PASSWORD +" AND p.deleted=false")

public class Person implements Serializable {

	private static final long serialVersionUID = -8843283246151774628L;

	// JPA Queries:
	public static final String QUERY_FIND_ALL_PERSON="QUERY_FIND_ALL_PERSON";
	public static final String QUERY_FIND_FILTERED_PERSON="QUERY_FIND_FILTERED_PERSON";
	public static final String QUERY_FIND_PERSON_BY_EMAIL_AND_PASSWORD="QUERY_FIND_PERSON_BY_EMAIL_AND_PASSWORD";
	public static final String QUERY_FIND_PERSON_BY_EMAIL="QUERY_FIND_PERSON_BY_EMAIL";
	public static final String QUERY_CHECK_PERSON_PRINCIPALS="QUERY_CHECK_PERSON_PRINCIPALS";
	
	// Parameters:
	public static final String PARAM_EMAIL= "PARAM_EMAIL";
	public static final String PARAM_PASSWORD= "PARAM_PASSWORD";
	public static final String PARAM_FILTER= "PARAM_FILTER";
	
	// Regex
	public static final String PATTERN_EMAIL="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
			+"[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
			+"(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private Long id;

	@Column(name="VORNAME")
	@Pattern(regexp = "^[A-Za-z0-9äÄöÖüÜ]+$")
	@Size( max=45)
	private String firstName;

	@Column(name="NAME")
	@Pattern(regexp = "^[A-Za-z0-9]+$")
	@Size( max=45)
	private String name;

	@Column(name="EMAIL")
	@Pattern(regexp=PATTERN_EMAIL,
	message="{warn.email.invalid}")
	@Size( max=45)
	private String email;

	@JsonIgnore
	@Column(name="PASSWORD")
	@Pattern(regexp = "^[A-Za-z0-9äÄöÖüÜ]+$")
	@Size( max=45)
	private  String password;
	
	@Column(name="DELETED")
	@JsonIgnore
	private  boolean deleted;
	
	@JsonIgnore
	@Column(name="ACTIVE")
	private boolean active;
	
	@JsonIgnore
	@Column(name="LANGUAGE")
	@Pattern(regexp = "^(DE|FR)$")
	@Size(min = 2, max=2)
	private String language;
	
	@ManyToMany(fetch = FetchType.EAGER,cascade = { 
			CascadeType.ALL,CascadeType.MERGE, CascadeType.PERSIST
		})
	@JoinTable(name = "USER_ROLLEN",
		    joinColumns = @JoinColumn(name = "USER_ID"),
		    inverseJoinColumns = @JoinColumn(name = "ROLLEN_ID")
		)
	@JsonIgnore
	private Set<Rollen> rollen = new HashSet<>();

	// GETTER / SETTER

	public Long getId() {
		return id;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public Set<Rollen> getRollen() {
		return rollen;
	}

	public void setRollen(Set<Rollen> rollen) {
		this.rollen = rollen;
	}
	
	public void addRolle(Rollen rolle) {
		this.rollen.add(rolle);
	}
	
	public void removeRolle(Rollen rolle) {
		this.rollen.remove(rolle);
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}




	

}
