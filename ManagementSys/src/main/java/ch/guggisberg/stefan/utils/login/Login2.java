package ch.guggisberg.stefan.utils.login;


import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import ch.guggisberg.stefan.groupfitness.course.entities.Person;
import ch.guggisberg.stefan.groupfitness.course.services.PersonService;

import java.io.Serializable;

@Named
@SessionScoped
public class Login2 implements Serializable {

	@EJB
	private PersonService ps;
	private Person loggedPerson;
	
	private static final long serialVersionUID = -1791011323783036520L;
	public String logout() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/home.xhtml?faces-redirect=true";
	}
	
	public void setLocal() {
		loggedPerson = new Person();
		ps.getLoggedPerson().getLanguage();
	}


}
