package ch.guggisberg.stefan.groupfitness.course.utils;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import ch.guggisberg.stefan.groupfitness.course.exceptions.DayOfWeekOutOfRangeException;
import ch.guggisberg.stefan.groupfitness.course.exceptions.StartDateHigherThanEndDateException;


/**
 * Sucht alle Daten für einen bestimmten Wochentag zwischen zwei Daten.
 * 
 * Beispiel:
 * 
 * 2 für Dienstag
 * 
 * StartDatum 6.12.2018
 * EndDatum 23.12.2018
 * 
 * Liefert alle Dienstage zwischen 6.12.2018 und 23.12.2018
 * Also: 11.12.2018 / 18.12.2018
 * 
 * @author guggi229
 *
 */

public class DateGenerator implements Serializable {
	private static final long serialVersionUID = -1194293661898215784L;

	private Set<LocalDate> listOfDates = new HashSet<>();

	public Set<LocalDate> getAllDays(LocalDate startDate, LocalDate endDate, int dayOfWeek) throws StartDateHigherThanEndDateException, DayOfWeekOutOfRangeException{

		// Abfangen von falschen Daten
		LocalDate searchDate = startDate;
		if (endDate.isBefore(startDate)) {
			throw new StartDateHigherThanEndDateException();
		}
		if(dayOfWeek==0 || dayOfWeek>7) {
			throw new DayOfWeekOutOfRangeException();
		}

		// Suchen erstes Datum zu Tag dayOfWeek
		for (int i = 1; i <= 7; i++) {
			if (searchDate.getDayOfWeek().getValue() == dayOfWeek) {
				break;
			}
			searchDate=searchDate.plusDays(1);
		}

		// return leere Liste wenn in diesem Zeitraum kein Tag vorhanden ist.
		if (searchDate.isAfter(endDate)) {
			return listOfDates;
		}

		// Auflisten aller Daten für diesen Wochentag
		while(searchDate.isBefore(endDate)) {
			listOfDates.add(searchDate);
			searchDate = searchDate.plusDays(7);
		}
		return listOfDates;
	}

}
