package ch.guggisberg.stefan.usermanagement.rest;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ch.guggisberg.stefan.groupfitness.course.entities.PersonAuthenticationTrx;
import ch.guggisberg.stefan.groupfitness.course.exceptions.CrudServiceException;
import ch.guggisberg.stefan.groupfitness.course.services.PersonService;
import ch.guggisberg.stefan.usermanagement.model.generell.ResponeState;
import ch.guggisberg.stefan.usermanagement.model.responses.ResponseUserAuthentication;
import ch.guggisberg.stefan.groupfitness.course.entities.Person;

@Path("personAuth")
public class AuthenticationPerson implements Serializable {

	private static final long serialVersionUID = -3497164492738532493L;

	@EJB
	private PersonService ps;

	private Person p;
	private ResponseUserAuthentication responseUserAuthentication;

	/**
	 * Prüft ob Passwort / Email stimmen.
	 * 
	 * @param pAutgTrx
	 * @return true - wenn dieser User so existiert in der DB.
	 */
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ResponseUserAuthentication checkPersonAuth(@Valid PersonAuthenticationTrx pAutgTrx) {
		p = new Person();
		p.setEmail(pAutgTrx.getEmail());
		p.setPassword(pAutgTrx.getPassword());
		responseUserAuthentication = new ResponseUserAuthentication();

		try {
			if (ps.checkUserPrincipals((p))) {
				responseUserAuthentication.setResponseState(ResponeState.OK);
				responseUserAuthentication.setAuthenticated(true);
				responseUserAuthentication.setMessage("Auth ok"); 
				return responseUserAuthentication;
			}

		} catch (CrudServiceException e) {
			responseUserAuthentication.setAuthenticated(false);
			responseUserAuthentication.setMessage("Technischer Fehler");
			responseUserAuthentication.setResponseState(ResponeState.NOK);
		}

		responseUserAuthentication.setAuthenticated(false);
		responseUserAuthentication.setMessage("User / Kennwort Kombination falsch");
		responseUserAuthentication.setResponseState(ResponeState.OK);
		return responseUserAuthentication;
	}

}
