package ch.guggisberg.stefan.groupfitness.course.services;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import ch.guggisberg.stefan.groupfitness.course.entities.Rollen;

@Stateless
@LocalBean
@DeclareRoles({"GrpAdmin", "customer", "Admin", "instruktor","admin"})
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class RollenService extends BaseCrud<Rollen> {

	@RolesAllowed("GrpAdmin")
	@Override
	public Rollen find(Class<Rollen> type, Object id) {
		return super.find(type, id);
	}
	
}
