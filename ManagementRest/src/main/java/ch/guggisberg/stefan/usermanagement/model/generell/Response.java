package ch.guggisberg.stefan.usermanagement.model.generell;

import java.io.Serializable;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Response implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private ResponeState responseState;
	@Pattern(regexp = "^[A-Za-z0-9äÄöÖüÜ\\s]+$")
	@Size( max=45)
	private String message;
	
	public ResponeState getResponseState() {
		return responseState;
	}
	public void setResponseState(ResponeState responseState) {
		this.responseState = responseState;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	

}
