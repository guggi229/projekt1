package ch.guggisberg.stefan.userController;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import ch.guggisberg.stefan.base.BaseBean;
import ch.guggisberg.stefan.groupfitness.course.entities.ClassRun;
import ch.guggisberg.stefan.groupfitness.course.services.ClassRunService;
import ch.guggisberg.stefan.groupfitness.course.services.PersonService;



@Named
@SessionScoped
public class StatisticsAddController extends BaseBean implements Serializable {
	private static final long serialVersionUID = 9063638083886397569L;

	@EJB
	private ClassRunService crs;
	
	@EJB
	private PersonService ps;

	
	public List<ClassRun> getMyClassRuns(){
		return	crs.getMyClassRunsInDateRange(ps.getLoggedPerson().getId(), LocalDate.now(),LocalDate.now());
	}


}
