package ch.guggisberg.stefan.groupfitness.course.entities;

import java.io.Serializable;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class PersonAuthenticationTrx implements Serializable  {

	private static final long serialVersionUID = -5527758572252029051L;
	
	
	private String email;
	
	@Pattern(regexp = "^[A-Za-z0-9äÄöÖüÜ]+$")
	@Size( max=45)
	private String password;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	
	
}
