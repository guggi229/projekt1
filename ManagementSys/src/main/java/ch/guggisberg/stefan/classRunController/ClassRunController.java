package ch.guggisberg.stefan.classRunController;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.apache.log4j.Logger;

import ch.guggisberg.stefan.base.BaseBean;
import ch.guggisberg.stefan.groupfitness.course.entities.ClassRun;
import ch.guggisberg.stefan.groupfitness.course.entities.Kurs;
import ch.guggisberg.stefan.groupfitness.course.entities.Person;
import ch.guggisberg.stefan.groupfitness.course.entities.Room;
import ch.guggisberg.stefan.groupfitness.course.exceptions.DayOfWeekOutOfRangeException;
import ch.guggisberg.stefan.groupfitness.course.exceptions.StartDateHigherThanEndDateException;
import ch.guggisberg.stefan.groupfitness.course.services.ClassRunService;
import ch.guggisberg.stefan.groupfitness.course.services.ClassService;
import ch.guggisberg.stefan.groupfitness.course.services.PersonService;
import ch.guggisberg.stefan.groupfitness.course.services.RoomService;
import ch.guggisberg.stefan.groupfitness.course.utils.DateGenerator;


@Named
@SessionScoped
public class ClassRunController extends BaseBean implements Serializable {

	private static final long serialVersionUID = 6962723803915463465L;
	private static Logger log = Logger.getLogger(ClassRunController.class);

	private static final DayOfWeek[] DAY_OF_WEEK = DayOfWeek.values();

	@EJB
	private ClassService cs;

	@EJB
	private PersonService ps;

	@EJB
	private ClassRunService crs;

	@EJB
	private RoomService rs;

	private ClassRun classRun;
	private Set<ClassRun> multiClassRuns;
	private Set<LocalDate> multidates;
	private DateGenerator dg;

	private Date startDate;
	private Date endDate;
	private String startTime;
	private int duration;
	private int day;

	private Long personId;
	private Long roomId;
	private Long classtypId;
	
	//NAV
	
	private static final String NAVIGATE_TO_CLASS_RUN_MULTI_PREVIEW_LIST = "/grpAdmin/classRunViews/classRunMultiPreViewList";
	private static final String NAVIGATE_TO_CLASS_RUN_LIST = "/grpAdmin/classRunViews/classRunsList";
	private static final String NAVIGATE_TO_CLASS_RUN_EDIT = "/grpAdmin/classRunViews/classRunEdit";

	public String createMultiClassRun() throws ParseException {
		multiClassRuns =  new HashSet<>();
		multidates = new HashSet<>();
		dg = new DateGenerator();
		try {
			multidates = dg.getAllDays(startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(), endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(), day);
		} catch (StartDateHigherThanEndDateException e) {
			showGlobalErrorMessage("error.messages.startdate.older");
			log.info("Das Start Datum ist nach dem End Datum");
		} catch (DayOfWeekOutOfRangeException e) {
			// Sollte nicht passieren können. Da ist beim Client was schief gelaufen
			showGlobalErrorMessage("error.messages.hopla");
			log.error("User hat einen falschen Wochentag, ausserhalb von 1-7 ausgewählt");
		}

		for (LocalDate singleDate : multidates) {

			preCreateClassRun();
			classRun.setDate(singleDate);
			multiClassRuns.add(classRun);
		}

		return NAVIGATE_TO_CLASS_RUN_MULTI_PREVIEW_LIST;
	}
	public String createFromPreview() {
		for (ClassRun classRun : multiClassRuns) {
			crs.createClassRun(classRun);
		}
		showGlobalMessage("messages.class.run.created.multi");
		return NAVIGATE_TO_CLASS_RUN_LIST;
	}


	public void createClassRun() throws ParseException {
		preCreateClassRun();
		classRun.setDate(startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
		crs.createClassRun(classRun);
		showGlobalMessage("messages.class.run.created.single");
	}

	private void preCreateClassRun() {
		classRun = new ClassRun();
		Room room= rs.getRoom(roomId);
		Person person = ps.getPerson(personId);
		classRun.setStartTime((LocalTime.parse(startTime)));
		classRun.setRoom(room);
		classRun.setPerson(person);
		classRun.setDuration(duration);
		classRun.setKurs(cs.getKurs(classtypId));
	}

	public Set<ClassRun> getPreviewClassRuns(){
		return multiClassRuns;
	}

	public String removeClassRun(ClassRun cr) {
		multiClassRuns.remove(cr);
		return NAVIGATE_TO_CLASS_RUN_MULTI_PREVIEW_LIST;
	}

	public String editClassRun(ClassRun cr) {
		classRun= crs.getClassRunById(cr.getId());
		return NAVIGATE_TO_CLASS_RUN_EDIT;
	}
	
	public String deleteClassRun(ClassRun cr) {
		crs.delete(cr);
		showGlobalMessage("messages.class.run.deleted");
		return NAVIGATE_TO_CLASS_RUN_LIST;
	}
	
	public String updateClassRun(ClassRun cr) {
		crs.update(cr);
		showGlobalMessage("messages.class.run.mod");
		return NAVIGATE_TO_CLASS_RUN_LIST;
	}
	
	// Getter / Setter

	public ClassRun getClassRun() {
		return classRun;
	}

	public void setClassRun(ClassRun classRun) {
		this.classRun = classRun;
	}

	public List<Person> getAllPerson(){
		return ps.getAllPersons();
	}

	public List<Kurs> getAllClasses(){
		return cs.getFilteredClasses("");
	}

	public List<Room> getAllRooms(){
		return rs.getAllRooms(); 
	}

	public List<ClassRun> getAllClassRuns(){
		return crs.getAllClassRuns();
	}

	public int getDuration() {
		return duration;
	}


	public void setDuration(int duration) {
		this.duration = duration;
	}


	public Long getPersonId() {
		return personId;
	}


	public void setPersonId(Long personId) {
		this.personId = personId;
	}


	public Long getRoomId() {
		return roomId;
	}


	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}


	public Long getClasstypId() {
		return classtypId;
	}


	public void setClasstypId(Long classtypId) {
		this.classtypId = classtypId;
	}


	public Date getStartDate() {
		return startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public String getStartTime() {
		return startTime;
	}


	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}


	public DayOfWeek[] getDayOfWeeks() {
		return DAY_OF_WEEK;
	}

	public Date getEndDate() {
		return endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}


}
