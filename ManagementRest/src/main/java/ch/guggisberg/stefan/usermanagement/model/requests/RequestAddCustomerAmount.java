package ch.guggisberg.stefan.usermanagement.model.requests;

import java.io.Serializable;

public class RequestAddCustomerAmount implements Serializable {


	private static final long serialVersionUID = -5515519955270077432L;
	private Long classRunId;
	private int customerAmount;
	
	public Long getClassRunId() {
		return classRunId;
	}
	public void setClassRunId(Long classRunId) {
		this.classRunId = classRunId;
	}
	public int getCustomerAmount() {
		return customerAmount;
	}
	public void setCustomerAmount(int customerAmount) {
		this.customerAmount = customerAmount;
	}
	
	
}
