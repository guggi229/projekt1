package ch.guggisberg.stefan.usermanagement.model.requests;

import java.io.Serializable;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class RequestFilter implements Serializable{

	private static final long serialVersionUID = 5155846934328992290L;
	
	@Pattern(regexp = "^[A-Za-z0-9äÄöÖüÜ]+$")
	@Size( max=45)
	private String filter;
	private Integer startPosition;
	private Integer maxRows;
	
	public Integer getStartPosition() {
		return startPosition;
	}
	public void setStartPosition(Integer startPosition) {
		this.startPosition = startPosition;
	
	}
	public Integer getMaxRows() {
		return maxRows;
	}
	public void setMaxRows(Integer maxRows) {
		this.maxRows = maxRows;
	}
	
	public String getFilter() {
		return filter;
	}
	public void setFilter(String filter) {
		this.filter = filter;
	}
	
	
	
}
