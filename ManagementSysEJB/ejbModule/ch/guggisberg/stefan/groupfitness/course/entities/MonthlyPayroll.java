package ch.guggisberg.stefan.groupfitness.course.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import ch.guggisberg.stefan.groupfitness.course.model.State;

@Entity
@Table(name="MONTHLYPAYROLL")
@NamedQueries({

	@NamedQuery(name= MonthlyPayroll.QUERY_MONTHLY_PAYROLL, 
			query ="Select mp FROM MonthlyPayroll mp WHERE  mp.state=:" + MonthlyPayroll.PARAM_STATE)
})
public class MonthlyPayroll implements Serializable {

	private static final long serialVersionUID = -914428315192708269L;

	public static final String QUERY_MONTHLY_PAYROLL = "QUERY_MONTHLY_PAYROLL";
	public static final String PARAM_STATE = "PARAM_STATE";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private Long id;

	@Column(name="DATE")
	private LocalDate date;

	@Enumerated(EnumType.STRING)
	@Column(name = "STATUS")
	private State state;

	@OneToMany(mappedBy = "monthlyPayroll",fetch = FetchType.EAGER, 
			cascade = CascadeType.ALL, 
			orphanRemoval = true
			)
	private List<ClassRun> classRuns = new ArrayList<>();

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "PERSONID")
	private Person person;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public List<ClassRun> getClassRuns() {
		return classRuns;
	}

	public void setClassRuns(List<ClassRun> classRuns) {
		this.classRuns = classRuns;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}


	public BigDecimal getTotalHours() {
		double minutes=0;
		for(ClassRun classrun : this.classRuns) {
			minutes=minutes+classrun.getDuration();
		}
		return new BigDecimal(minutes/60);
	}


}
