package ch.guggisberg.stefan.usermanagement.model.requests;

import java.io.Serializable;
import java.time.LocalDate;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class RequestMyClassRunsInDateRange implements Serializable {

	private static final long serialVersionUID = -2458251275774497561L;
	
	@Pattern(regexp = "^[A-Za-z0-9äÄöÖüÜ]+$")
	@Size( max=45)
	private String startDate;
	
	@Pattern(regexp = "^[A-Za-z0-9äÄöÖüÜ]+$")
	@Size( max=45)
	private String endDate;
	
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	

	
	
}
