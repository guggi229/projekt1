package ch.guggisberg.stefan.statisticsclassruns.rest;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import ch.guggisberg.stefan.usermanagement.model.generell.ResponeState;
import ch.guggisberg.stefan.usermanagement.model.generell.Response;
import ch.guggisberg.stefan.usermanagement.model.requests.RequestAddCustomerAmount;
import ch.guggisberg.stefan.usermanagement.model.requests.RequestMyClassRunsInDateRange;
import ch.guggisberg.stefan.usermanagement.model.responses.ResponseClassRuns;
import ch.guggisberg.stefan.groupfitness.course.entities.ClassRun;
import ch.guggisberg.stefan.groupfitness.course.exceptions.WrongPersonException;
import ch.guggisberg.stefan.groupfitness.course.services.ClassRunService;
import ch.guggisberg.stefan.groupfitness.course.services.PersonService;

//Aufruf: http://localhost:8080/ManagementSysRest/rest/classrunstats



@Path("classrunstats")
public class ClassRunStatsRest implements Serializable{

	private static final long serialVersionUID = 5624385120255136418L;
	private static Logger log = Logger.getLogger(ClassRunStatsRest.class);
	private static DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy");

	@EJB 
	private ClassRunService crs;
	
	@EJB 
	private PersonService ps;
	
	private Response r;
	private ClassRun classRun;

	
	// Aufruf: http://group-fitness.ch/ManagementSysRest/rest/classrunstats
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response addCustomerAmount (RequestAddCustomerAmount requestAddCustomerAmount) {
		r= new Response();
		classRun = crs.getClassRunById(requestAddCustomerAmount.getClassRunId());
		classRun.setAmountCustomer(requestAddCustomerAmount.getCustomerAmount());

		try {
			crs.addcustomerAmount(classRun);
			r.setResponseState(ResponeState.OK);
			r.setMessage("Teilnehmeranzahl gespeichert");
		} catch (WrongPersonException e) {
			log.error("Dieser User darf die Operatio nicht durchführen", e);
			r.setResponseState(ResponeState.NOK);
			r.setMessage("Dieser User darf die Operatio nicht durchführen");
		}
		return r;
	}
	
	// Aufruf: http://group-fitness.ch/ManagementSysRest/rest/classrunstats/classrunsindaterange
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("classrunsindaterange")
	public ResponseClassRuns getMyClassRunsInDateRange(RequestMyClassRunsInDateRange request) {
		String startDate = request.getStartDate();
		String endDate = request.getEndDate();
		ResponseClassRuns response = new ResponseClassRuns();
		try {
			response.setClassRuns(crs.getMyClassRunsInDateRange(ps.getLoggedPerson().getId(), LocalDate.parse(startDate, FORMATTER), LocalDate.parse(endDate, FORMATTER)));
			response.setResponseState(ResponeState.OK);
			response.setMessage("Liste geladen");
		} 
		
		catch (DateTimeParseException e) {
			response.setResponseState(ResponeState.NOK);
			response.setMessage("Datum hat falsches Format");
		}
		catch (Exception e) {
			response.setResponseState(ResponeState.NOK);
			response.setMessage("Technischer Fehler");
		}
	
		return response;
	}
	
}
