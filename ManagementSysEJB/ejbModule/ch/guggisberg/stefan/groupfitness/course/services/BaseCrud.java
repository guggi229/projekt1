package ch.guggisberg.stefan.groupfitness.course.services;

import java.util.List;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections4.MapUtils;

import ch.guggisberg.stefan.groupfitness.course.exceptions.CrudServiceException;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class BaseCrud<T>  {

	@PersistenceContext
	protected EntityManager entityManager;


	// Standard CRUD Services
	// =======================


	public T persist(T entity) {
		entityManager.persist(entity);
		entityManager.flush();
		return entity;
	}

	public T find(Class<T> type, Object id) {
		return entityManager.find(type, id);
	}

	public T update(T entity) {
		return entityManager.merge(entity);
	}

	public boolean delete(Class<T> type, Object id) {
		T entity = entityManager.find(type, id);
		if (entity == null) {
			return false;
		}
		entityManager.remove(entity);
		return true;
	}

	// Erweiterte CRUD Service
	// =======================
	@SuppressWarnings({ "unchecked", "hiding" })
	public <T> List<T>  findListResultNamedQuery(String name) {
		Query query = entityManager.createNamedQuery(name);	
		return (List<T>) query.getResultList();
	}
	@SuppressWarnings({ "unchecked", "hiding" })
	public <T> T findSingleResultNamedQuery(String name, Map <String, Object> params) {
		Query query = entityManager.createNamedQuery(name);	
		for (String paramName : params.keySet()) {
			query.setParameter(paramName, params.get(paramName));
		}
		return (T) query.getSingleResult();
	}

	@SuppressWarnings({ "unchecked", "hiding" })
	public int findSingleResultNumberNamedQuery(String name, Map <String, Object> params) {
		Query query = entityManager.createNamedQuery(name);	
		if (params != null) {
			Iterator var8 = params.entrySet().iterator();

			while(var8.hasNext()) {
				Entry<String, Object> param = (Entry)var8.next();
				query.setParameter((String)param.getKey(), param.getValue());
			}
		}
		return (int) query.getSingleResult();
	}

	@SuppressWarnings("hiding")
	public <T> List<T> findByNamedQuery(String name, Map<String, Object> params ){
		return findByNamedQuery(name, params, null, null,null);
	}

	@SuppressWarnings("hiding")
	public <T> List<T> findByNamedQuery(String name, Map<String, Object> params, LockModeType lockModeType ){
		return findByNamedQuery(name, params, null, null,lockModeType);
	}

	@SuppressWarnings("hiding")
	public <T> List<T> findByNamedQuery(String name ){
		return findByNamedQuery(name, null, null, null,null);
	}



	@SuppressWarnings({ "hiding", "unchecked" })
	public <T> List<T> findByNamedQuery(String name, Map<String, Object> params, Integer startPosition, Integer maxRowse ){
		return findByNamedQuery(name, params, startPosition, maxRowse, null);
	}

	@SuppressWarnings({ "hiding", "unchecked" })
	public <T> List<T> findByNamedQuery(String name, Map<String, Object> params, Integer startPosition, Integer maxRows ,LockModeType lockModeTyp){

		Query query = entityManager.createNamedQuery(name);
		if (params != null) {
			Iterator var8 = params.entrySet().iterator();

			while(var8.hasNext()) {
				Entry<String, Object> param = (Entry)var8.next();
				query.setParameter((String)param.getKey(), param.getValue());
			}
		}

		if (lockModeTyp != null) {
			query.setLockMode(lockModeTyp);
		}

		if (startPosition != null) {
			query.setFirstResult(startPosition);
		}

		if (maxRows != null) {
			query.setMaxResults(maxRows);
		}

		return query.getResultList();

	}
	public Long executeNamedQuery(String name) throws CrudServiceException {
		return this.executeNamedQuery(name, (Map)null, (LockModeType)null, (Map)null);
	}

	public Long executeNamedQuery(String name, LockModeType lockMode, Map<String, Object> queryHints) throws CrudServiceException {
		return this.executeNamedQuery(name, (Map)null, lockMode, queryHints);
	}

	public Long executeNamedQuery(String name, Map<String, Object> params) throws CrudServiceException {
		return this.executeNamedQuery(name, params, (LockModeType)null, (Map)null);
	}


	public Long executeNamedQuery(String name, Map<String, Object> params, LockModeType lockMode, Map<String, Object> queryHints) throws CrudServiceException {

		Query query = entityManager.createNamedQuery(name);
		if (params != null) {
			Iterator var6 = params.entrySet().iterator();

			while(var6.hasNext()) {
				Entry<String, Object> param = (Entry)var6.next();
				query.setParameter((String)param.getKey(), param.getValue());
			}
		}

		if (lockMode != null) {
			query.setLockMode(lockMode);
		}

		if (MapUtils.isNotEmpty(queryHints)) {
			queryHints.forEach((hint, value) -> {
				query.setHint(hint, value);
			});
		}

		return (Long) query.getSingleResult();

	}


}
