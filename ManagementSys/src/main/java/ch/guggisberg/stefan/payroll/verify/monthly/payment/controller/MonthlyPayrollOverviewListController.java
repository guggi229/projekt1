package ch.guggisberg.stefan.payroll.verify.monthly.payment.controller;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.apache.log4j.Logger;

import ch.guggisberg.stefan.base.BaseBean;
import ch.guggisberg.stefan.groupfitness.course.entities.ClassRun;
import ch.guggisberg.stefan.groupfitness.course.entities.MonthlyPayroll;
import ch.guggisberg.stefan.groupfitness.course.model.State;
import ch.guggisberg.stefan.groupfitness.course.services.ClassRunService;
import ch.guggisberg.stefan.groupfitness.course.services.MonthlyPayrollService;


/**
 * Kontroller für die Übersicht der eingelieferten Stundenblätter.
 * 
 * @author guggi229
 *
 */
@Named
@SessionScoped
public class MonthlyPayrollOverviewListController extends BaseBean implements Serializable{

	private static final long serialVersionUID = 297846352091985793L;
	private static Logger log = Logger.getLogger(MonthlyPayrollOverviewListController.class);

	private MonthlyPayroll monthlyPayroll;
	private List<ClassRun> classRuns;

	@EJB
	private MonthlyPayrollService mps;

	@EJB
	private ClassRunService crs;

	//NAV

	private static final String NAVIGATE_TO_MONTHLY_PAYROLL_DETAIL_VIEW = "/grpAdmin/payrollViews/monthlyPayrollDetailView";
	private static final String NAVIGATE_TO_MONTHLY_PAYROLL_OVERVIEW_LIST_VIEW = "/grpAdmin/payrollViews/monthlyPayrollOverviewListView";

	public List<MonthlyPayroll> getPayRollList(){
		return mps.getVerifyList(State.pending);
	}

	public String verifyDetailView (MonthlyPayroll monthlyPayroll) {
		this.monthlyPayroll = mps.getMonthlyPayroll(monthlyPayroll.getId());
		this.classRuns=monthlyPayroll.getClassRuns();
		return NAVIGATE_TO_MONTHLY_PAYROLL_DETAIL_VIEW;
	}

	public List<ClassRun> getClasses(){
		// Nicht löschen. Hier gibt es ein spanndes Problem mit JSF Lifesycle.
		// Das möchte ich noch genauer anschauen.
		// Dirty Read?
		System.out.println("Size?");
		System.out.println(monthlyPayroll.getClassRuns().size());
		return classRuns;
	}

	public String release() {
		monthlyPayroll.setState(State.approved);
		mps.update(monthlyPayroll);
		showGlobalMessage("messages.finance.reday2check");
		return NAVIGATE_TO_MONTHLY_PAYROLL_OVERVIEW_LIST_VIEW;
	}

	// Getter Setter

	public MonthlyPayroll getMonthlyPayroll() {
		return monthlyPayroll;
	}

	public void setMonthlyPayroll(MonthlyPayroll monthlyPayroll) {
		this.monthlyPayroll = monthlyPayroll;
	}


}
