$(function() {

  // BONUS TIP: disable a column using jQuery data directly
  // but do it before the table initializes
  // this code disables the sort on the "Date" column
  $("table thead th:eq(5)").data("sorter", false);

  $("table").tablesorter({
    theme : 'blue',

    headers: {
      // disable sorting of the first & second column - before we would have to had made two entries
      // note that "first-name" is a class on the span INSIDE the first column th cell
      '.first-name, .last-name' : {
        // disable it by setting the property sorter to false
        sorter: false
      }
    }
  });
});

$('#radioBtn a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
})