package ch.guggisberg.stefan.groupfitness.course.exceptions;

public class StartDateHigherThanEndDateException extends Exception {
	private static final long serialVersionUID = 458641477032831101L;

	public StartDateHigherThanEndDateException() {
		super("Das StartDatum ist grösser als das EndDatum!");
	}
	
}




