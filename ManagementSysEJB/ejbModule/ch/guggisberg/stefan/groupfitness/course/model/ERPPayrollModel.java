package ch.guggisberg.stefan.groupfitness.course.model;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


/**
 *
 * Erstellt die Monatsabrechnung f�r das ERP System.
 *
 * Prüft eine einfache Plausibilit�t.
 *
 * @author guggisberste
 *
 */

public class ERPPayrollModel {

	private Long id;

	private LocalDate salaryPeriod;

	private BigDecimal salaryTyp;

	private BigDecimal amountOfDays;

	private BigDecimal amountOfHours;

	private BigDecimal unusedSpaceInTheMiddle;

	private BigDecimal amountOfsalary;

	private DecimalFormat idPattern = new DecimalFormat("0000");
	private DateTimeFormatter salaryPeriodPattern = DateTimeFormatter.ofPattern("YYYYMM");
	private DecimalFormat salaryTypPattern = new DecimalFormat("000");
	private DecimalFormat amountOfDaysPattern = new DecimalFormat("0000000.00");
	private DecimalFormat amountOfHoursPattern = new DecimalFormat("0000000.00");
	private DecimalFormat unusedSpaceInTheMiddlePattern = new DecimalFormat("0000000.00");
	private DecimalFormat amountOfsalaryPattern = new DecimalFormat("0000000.00");

	public ERPPayrollModel(Long id, LocalDate salaryPeriod, BigDecimal salaryTyp, BigDecimal amountOfHours){
		if (id > 999 || id <0){
			throw new IllegalArgumentException();
		}

		if (salaryTyp.compareTo(new BigDecimal("999")) > 0){
			throw new IllegalArgumentException();
		}

		if (amountOfHours.compareTo(new BigDecimal("200")) > 0){
			throw new IllegalArgumentException();
		}

		this.id=id;
		this.salaryPeriod = LocalDate.now();
		this.salaryTyp = salaryTyp;
		this.amountOfHours = amountOfHours;
		amountOfDays = new BigDecimal("0");
		amountOfsalary = new BigDecimal("0");

	}

	private String getIdERPFormat(){
		return idPattern.format(id);
	}

	private String getSalaryDateERPFormat(){
		return salaryPeriod.format(salaryPeriodPattern);
	}

	private String getSalaryTypERPFormat(){
		return salaryTypPattern.format(salaryTyp);
	}

	private String getAmountDaysERPFormat(){
		return amountOfDaysPattern.format(amountOfDays);
	}

	private String getAmountHoursERPFormat(){
		return amountOfHoursPattern.format(amountOfHours);
	}

	private String getUnusedSpaceInTheMiddleERPFormat(){
		unusedSpaceInTheMiddle = new BigDecimal("0");
		return unusedSpaceInTheMiddlePattern.format(unusedSpaceInTheMiddle);
	}

	private String getAmountOfsalaryERPFormat(){
		return amountOfsalaryPattern.format(amountOfsalary);
	}

	private String getUnusedSpaceAtEndERPFormat(){
		String s = "0";
		int n = 60;
		String unusedText = IntStream.range(0, n).mapToObj(i -> s).collect(Collectors.joining(""));
		return unusedText;
	}

	/**
	 *  Liefert das Stundenblatt im ERP Format zur�ck.
	 *
	 *  Monat/Person
	 *
	 * @return ERP PayrollFormat
	 */
	public final String getERPPayrollModel(){
		return MessageFormat.format("{0}{1}{2}{3}{4}{5}{6}{7}", getIdERPFormat(),getSalaryDateERPFormat(), getSalaryTypERPFormat(), getAmountDaysERPFormat(), getAmountHoursERPFormat(),getUnusedSpaceInTheMiddleERPFormat(),getAmountOfsalaryERPFormat(),getUnusedSpaceAtEndERPFormat() );
	}

}
