package ch.guggisberg.stefan.groupfitness.course.test;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.Assert;
import org.junit.Test;

import ch.guggisberg.stefan.groupfitness.course.model.ERPPayrollModel;

public class TestERPExporter {


	private final static String REGEX  = "^[0-9]{4}20[0-9]{4}[0-9]{3}[0]{7}.{1}[0]{2}[0]{4}[0-9]{3}.[0-9]{2}[0]{7}.[0]{7}[0]{2}.[0]{2}[0]{60}$";
	@Test
	public void test() {
		ERPPayrollModel model = new ERPPayrollModel(100L, LocalDate.now(), new BigDecimal("100"), new BigDecimal("200.0"));
		String erpInput = model.getERPPayrollModel();
		Assert.assertEquals(true, erpInput.matches(REGEX));
	}

	@Test(expected = IllegalArgumentException.class)
	public void outOfRangeIDTest(){
		@SuppressWarnings("unused")
		ERPPayrollModel model = new ERPPayrollModel(1000L, LocalDate.now(), new BigDecimal("100"), new BigDecimal("200"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void outOfRangeSalaryTypTest(){
		@SuppressWarnings("unused")
		ERPPayrollModel model = new ERPPayrollModel(100L, LocalDate.now(), new BigDecimal("1000"), new BigDecimal("100"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void outOfRangeAmountOfHoursTest()	{
		@SuppressWarnings("unused")
		ERPPayrollModel model = new ERPPayrollModel(100L, LocalDate.now(), new BigDecimal("100"), new BigDecimal("200.5"));
	}

}
