package ch.guggisberg.stefan.usermanagement.model.requests;

import java.io.Serializable;

public class RequestPersonDeleteTrxModel implements Serializable{

	private static final long serialVersionUID = -7700949603691739038L;

	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
