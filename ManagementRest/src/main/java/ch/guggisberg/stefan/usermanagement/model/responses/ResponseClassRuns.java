package ch.guggisberg.stefan.usermanagement.model.responses;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import ch.guggisberg.stefan.groupfitness.course.entities.ClassRun;
import ch.guggisberg.stefan.usermanagement.model.generell.ResponeState;

public class ResponseClassRuns  implements Serializable {

	private static final long serialVersionUID = -4408042738573574765L;
	private List<ClassRun> classRuns;
	
	private ResponeState responseState;
	@Pattern(regexp = "^[A-Za-z0-9äÄöÖüÜ\\s]+$")
	@Size( max=45)
	private String message;
	
	public List<ClassRun> getClassRuns() {
		return classRuns;
	}
	public void setClassRuns(List<ClassRun> classRuns) {
		this.classRuns = classRuns;
	}
	public ResponeState getResponseState() {
		return responseState;
	}
	public void setResponseState(ResponeState responseState) {
		this.responseState = responseState;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
}
