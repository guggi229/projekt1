package ch.guggisberg.stefan.usermanagement.model.responses;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import ch.guggisberg.stefan.groupfitness.course.entities.Person;
import ch.guggisberg.stefan.usermanagement.model.generell.ResponeState;

public class ResponsePersonListe implements Serializable {

	private static final long serialVersionUID = -7123036938367106205L;

	private List<Person> person =new ArrayList<>();
	private ResponeState responseState;
	@Pattern(regexp = "^[A-Za-z0-9äÄöÖüÜ\\s]+$")
	@Size( max=45)
	private String message;
	public List<Person> getPerson() {
		return person;
	}
	public void setPerson(List<Person> person) {
		this.person = person;
	}
	public ResponeState getResponseState() {
		return responseState;
	}
	public void setResponseState(ResponeState responseState) {
		this.responseState = responseState;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	

	
}
