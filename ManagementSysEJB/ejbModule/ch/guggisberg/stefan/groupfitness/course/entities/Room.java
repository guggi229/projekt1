package ch.guggisberg.stefan.groupfitness.course.entities;

import java.io.Serializable;
import java.util.Locale;

import javax.faces.context.FacesContext;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.guggisberg.stefan.groupfitness.course.model.SupprtedLanguages;

@Entity
@Table(name="ROOM")
@NamedQueries({
	@NamedQuery(name= Room.QUERY_FIND_ALL_ROOMS, 
			query ="SELECT r FROM Room r WHERE  r.deleted=false")
})
public class Room  implements Serializable {

	private static final long serialVersionUID = 4457450544294952312L;
	public static final String QUERY_FIND_ALL_ROOMS="QUERY_FIND_ALL_ROOMS";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private Long id;

	@JsonIgnore
	@Column(name="NAMEDE")
	private String nameDe;

	@JsonIgnore
	@Column(name="NAMEFR")
	private String nameFr;

	@Column(name="DELETED")
	private  boolean deleted;

	@Transient
	@JsonIgnore
	private  String name;
	// Getter / Setter

	/**
	 * Gibt den Übersetzten Name an.
	 * @param l
	 * @return String - I18n
	 */
	public String getName() {
		Locale l = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		if (l==null || l.getLanguage().equals(new Locale(SupprtedLanguages.de.toString()).getLanguage() )) return nameDe;
		return nameFr;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNameDe() {
		return nameDe;
	}

	public void setNameDe(String nameDe) {
		this.nameDe = nameDe;
	}

	public String getNameFr() {
		return nameFr;
	}

	public void setNameFr(String nameFr) {
		this.nameFr = nameFr;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}




}
