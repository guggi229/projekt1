package ch.guggisberg.stefan.payroll.monthly.payroll.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.apache.log4j.Logger;

import ch.guggisberg.stefan.base.BaseBean;
import ch.guggisberg.stefan.groupfitness.course.entities.ClassRun;
import ch.guggisberg.stefan.groupfitness.course.entities.MonthlyPayroll;
import ch.guggisberg.stefan.groupfitness.course.model.State;
import ch.guggisberg.stefan.groupfitness.course.services.ClassRunService;
import ch.guggisberg.stefan.groupfitness.course.services.MonthlyPayrollService;
import ch.guggisberg.stefan.groupfitness.course.services.PersonService;

@Named
@SessionScoped
public class MonthlyPayrollController extends BaseBean  {

	private static final long serialVersionUID = 3443807464321189414L;
	private static Logger log = Logger.getLogger(MonthlyPayrollController.class);

	private LocalDate deliveryDate = LocalDate.now();


	@EJB
	private ClassRunService crs;

	@EJB
	private PersonService ps;

	@EJB
	private MonthlyPayrollService mps;
	
	private MonthlyPayroll monthlyPayroll;

	//NAV
	
	private static final String NAVIGATE_TO_MONTHLY_GRADUATION = "/instructor/monthlyGraduation";
	
	@PostConstruct
	public void init() {
		multiClassRuns= crs.getMyOpenClassRuns();
	}

	private List<ClassRun> multiClassRuns =  new ArrayList<>();


	public String removeFromMonthlyPayroll(ClassRun cr) {
		multiClassRuns.remove(cr);
		return NAVIGATE_TO_MONTHLY_GRADUATION;
	}

	public List<ClassRun> getMultiClassRuns() {
		return multiClassRuns;
	}

	public void setMultiClassRuns(List<ClassRun> multiClassRuns) {
		this.multiClassRuns = multiClassRuns;
	}
/*
 * Der Instruktor liefert hier seine Stunden ab. Die Daten werden in das Stundenblatt gespeichert.
 * 
 * 
 */
	public String createPayRollDoc() {
		try {
			monthlyPayroll = new MonthlyPayroll();
			monthlyPayroll.setPerson(ps.getLoggedPerson());
			monthlyPayroll.setState(State.pending);
			monthlyPayroll.setDate(LocalDate.now());
			monthlyPayroll = mps.createMonthlyPayRoll(monthlyPayroll);
			for (ClassRun classRun : multiClassRuns) {
				classRun.setState(State.pending);
				classRun.setMonthlyPayroll(monthlyPayroll);
				crs.update(classRun);
			}
			multiClassRuns.clear();
			showGlobalMessage("messages.person.payroll.delivered");
		} catch (Exception e) {
			showGlobalErrorMessage("error.messages.hopla");
			log.error(e);
		}

		return NAVIGATE_TO_MONTHLY_GRADUATION;
	}

	
	// Getter Setter
	
	public  LocalDate getDeliveryDate() {
		return deliveryDate;
	}

	public MonthlyPayroll getMonthlyPayroll() {
		return monthlyPayroll;
	}

	public void setMonthlyPayroll(MonthlyPayroll monthlyPayroll) {
		this.monthlyPayroll = monthlyPayroll;
	}






}
