package ch.guggisberg.stefan.groupfitness.course.services;

import java.util.HashMap;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import ch.guggisberg.stefan.groupfitness.course.entities.Person;
import ch.guggisberg.stefan.groupfitness.course.entities.PersonAuthenticationTrx;
import ch.guggisberg.stefan.groupfitness.course.exceptions.CrudServiceException;

@Stateless
@LocalBean
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class PersonPrincipalsService extends BaseCrud<Person>{

	@EJB
	private PersonService ps;
	
	public final Boolean checkPersonPrincipals(PersonAuthenticationTrx p) throws CrudServiceException {
		HashMap<String, Object> params = new HashMap<>();
//      int count = ((Number)em.createNamedQuery("Charakteristika.findAllCount").getSingleResult()).intValue();

		int count = findSingleResultNumberNamedQuery(Person.QUERY_CHECK_PERSON_PRINCIPALS,null);
		return (count > 0 ) ? true : false;
	}
}
