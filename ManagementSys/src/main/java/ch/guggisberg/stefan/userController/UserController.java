package ch.guggisberg.stefan.userController;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.log4j.Logger;

import ch.guggisberg.stefan.base.BaseBean;
import ch.guggisberg.stefan.groupfitness.course.entities.Person;
import ch.guggisberg.stefan.groupfitness.course.exceptions.NewPasswordsMisMachtesException;
import ch.guggisberg.stefan.groupfitness.course.exceptions.PasswordWrongException;
import ch.guggisberg.stefan.groupfitness.course.services.ClassService;
import ch.guggisberg.stefan.groupfitness.course.services.PersonService;

@Named
@SessionScoped
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class UserController extends BaseBean implements Serializable{
	private static final long serialVersionUID = 4120644572130464132L;
	private static Logger log = Logger.getLogger(UserController.class);

	private Person person = new Person();

	
	//NAV

	private static final String NAVIGATE_TO_USER_LIST = "/grpAdmin/userViews/userList";
	private static final String NAVIGATE_TO_USER_EDIT = "/grpAdmin/userViews/userEdit";
	
		
	@EJB
	private PersonService ps;
	
	@EJB
	private ClassService cs;

	@Pattern(regexp = "^[A-Za-z0-9äÄöÖüÜ]+$")
	@Size(min=1, max=45)
	private String newPassword;

	@Pattern(regexp = "^[A-Za-z0-9äÄöÖüÜ]+$")
	@Size(min=1, max=45)
	private String newRepeatedPassword;

	@Pattern(regexp = "^[A-Za-z0-9äÄöÖüÜ]+$")
	@Size(min = 1, max=45)
	private String oldPassword;

	@Pattern(regexp = "^(DE|FR)$")
	@Size(min = 2, max=2)
	private String lang;

	public String createUser() {
		showGlobalMessage(MessageFormat.format("messages.user.created", person.getFirstName()));
		ps.createPerson(person);
		person=null;
		return NAVIGATE_TO_USER_LIST;
	}

	public String deleteView(Person p) {
		ps.remove(p.getId());
		showGlobalMessage("messages.user.deleted");
		return NAVIGATE_TO_USER_LIST;
	}

	public String update() {
		person= ps.updatePerson(person);
		showGlobalMessage("messages.user.changed");
		return NAVIGATE_TO_USER_LIST;
	}

	public String editView(Person p) {
		this.person = ps.find(Person.class, p.getId());
		return NAVIGATE_TO_USER_EDIT;
	}

	public String search() {
		return NAVIGATE_TO_USER_LIST;
	}

	/**
	 * Ändert das Kennwort
	 * @return
	 */
	public String changePassword() {
		try {
			ps.changePassword(newPassword, newRepeatedPassword, oldPassword);
			showGlobalMessage("messages.user.password.changed");
		} catch (NewPasswordsMisMachtesException e) {
			showGlobalErrorMessage("messages.user.password.doesnt.match");
			log.info(e);
		} catch (PasswordWrongException e) {
			showGlobalErrorMessage("messages.user.password.wrong");
			log.info(e);
		}
		oldPassword = "";
		newPassword = "";
		newRepeatedPassword = "";
		return "/person/settings";
	}

	/**
	 * Ändert die Sprache in Faces Context sowie auf der DB.
	 * @return
	 */
	public String changeLang() {
		person = ps.getLoggedPerson();
		if (lang == null ||lang.equals("de")) {
			FacesContext.getCurrentInstance().getViewRoot().setLocale(Locale.GERMAN);
		}
		else {
			FacesContext.getCurrentInstance().getViewRoot().setLocale(Locale.FRENCH);
		}
		person.setLanguage(lang);
		person = ps.update(person);
		showGlobalMessage("messages.user.lang.changed");
		return "/person/settings";
	}


	// Gette Setter
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;	
	}

	public List<Person> getFilteredPersons(){
		return ps.getFilteredPersons(filter);
	}

	public List<Person> getAllPerson(){
		return ps.getAllPersons();
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getNewRepeatedPassword() {
		return newRepeatedPassword;
	}

	public void setNewRepeatedPassword(String newRepeatedPassword) {
		this.newRepeatedPassword = newRepeatedPassword;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

}
