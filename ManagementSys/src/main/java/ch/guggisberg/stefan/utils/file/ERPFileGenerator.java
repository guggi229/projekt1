package ch.guggisberg.stefan.utils.file;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.List;

import javax.ejb.EJB;


import org.omnifaces.util.Faces;

import ch.guggisberg.stefan.groupfitness.course.entities.MonthlyPayroll;
import ch.guggisberg.stefan.groupfitness.course.model.ERPPayrollModel;
import ch.guggisberg.stefan.groupfitness.course.services.MonthlyPayrollService;

/**
 * Diese Hilfklasse generiert das zu exportierende File in das ERP System.
 * @author guggi229
 *
 */



public class ERPFileGenerator implements Serializable {

	private static final long serialVersionUID = 8199643584662480638L;

	@EJB
	private MonthlyPayrollService mps;
	private List<MonthlyPayroll> monthlypayrollList;
	private StringBuilder sb;

	public ERPFileGenerator(List<MonthlyPayroll> monthlypayrollList) {
		this.monthlypayrollList = monthlypayrollList;
	}

	private void generateERPStream() throws IOException {
		sb = new StringBuilder();
		for (MonthlyPayroll monthlyPayroll : monthlypayrollList) {
			ERPPayrollModel erpModel = new ERPPayrollModel(monthlyPayroll.getPerson().getId(), LocalDate.now(), new BigDecimal("200"), monthlyPayroll.getTotalHours());
			sb.append(erpModel.getERPPayrollModel());
			sb.append(System.lineSeparator());
		}
	}

	public void generateTheExportFile() throws IOException {
		generateERPStream();
		InputStream stream = new ByteArrayInputStream(sb.toString().getBytes(StandardCharsets.UTF_8));
		try {
			Faces.sendFile(stream, MessageFormat.format("Payroll-{0}", LocalDate.now()), false);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
