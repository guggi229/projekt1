package ch.guggisberg.stefan.usermanagement.rest.utils;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

@Path("heartbeat")
public class HeartBeat {
	// Aufruf lokal:  http://localhost:8080/ManagementSysRest/rest/heartbeat
	@GET
	@Produces({MediaType.TEXT_PLAIN})
	public  String check(@Context HttpHeaders headers) {
		return "ok";
	}

}
