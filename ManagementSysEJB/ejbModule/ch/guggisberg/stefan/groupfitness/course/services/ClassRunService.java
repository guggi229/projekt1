package ch.guggisberg.stefan.groupfitness.course.services;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.annotation.security.DeclareRoles;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import ch.guggisberg.stefan.groupfitness.course.entities.ClassRun;
import ch.guggisberg.stefan.groupfitness.course.exceptions.WrongPersonException;
import ch.guggisberg.stefan.groupfitness.course.model.State;

@Stateless
@LocalBean
@DeclareRoles({"GrpAdmin", "customer", "Admin", "instruktor","admin"})
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ClassRunService extends BaseCrud<ClassRun>  {
	
	
	@Resource 
	private SessionContext ctx;
	
	@EJB
	private PersonService ps;
	
	@RolesAllowed("GrpAdmin")
	public List<ClassRun> getAllClassRuns(){
		return findByNamedQuery(ClassRun.QUERY_FIND_ALL_CLASS_RUNS);
	}
	
	@RolesAllowed("instruktor")
	public List<ClassRun> getMyClassRuns(Long userId){
		HashMap<String, Object> params = new HashMap<>();
		params.put(ClassRun.PARAM_FILTER_PERSON_ID, userId);
		return findByNamedQuery(ClassRun.QUERY_FIND_MY_CLASS_RUNS,params);
	}
	
	@RolesAllowed("instruktor")
	public List<ClassRun> getMyClassRunsInDateRange(Long userId, LocalDate startDate, LocalDate endDate){
		HashMap<String, Object> params = new HashMap<>();
		params.put(ClassRun.PARAM_FILTER_PERSON_ID, userId);
		params.put(ClassRun.PARAM_FILTER_START_DATE, startDate);
		params.put(ClassRun.PARAM_FILTER_END_DATE, endDate);
		return findByNamedQuery(ClassRun.QUERY_FIND_MY_CLASS_RUNS_IN_DATE_RANGE,params);
	}
	
	@RolesAllowed("instruktor")
	public List<ClassRun> getMyClassRunsInDateRange(Long userId, LocalDate startDate, LocalDate endDate, boolean booked){
		HashMap<String, Object> params = new HashMap<>();
		params.put(ClassRun.PARAM_FILTER_PERSON_ID, userId);
		params.put(ClassRun.PARAM_FILTER_START_DATE, startDate);
		params.put(ClassRun.PARAM_FILTER_END_DATE, endDate);
		params.put(ClassRun.PARAM_FILTER_IS_BOOKED, booked);
		return findByNamedQuery(ClassRun.QUERY_FIND_MY_CLASS_RUNS_IN_DATE_RANGE_AND_BOOKED,params);
	}
	
	@RolesAllowed("GrpAdmin")
	public void createClassRun(ClassRun classRun) {
		entityManager.persist(classRun);
	}
	
	@RolesAllowed({"GrpAdmin","instruktor"})
	@Override
	public ClassRun update(ClassRun cr) {
		return super.update(cr);
	}
	
	@RolesAllowed({"GrpAdmin","instruktor"})
	@Override
	public ClassRun find(Class<ClassRun> type, Object id) {
		return super.find(type, id);
	}
	
	@PermitAll
	public ClassRun getClassRunById(Long id) {
		return find(ClassRun.class, id);
	}
	/**
	 *  Liefert alle ClassRuns des eingeloggten Users mit dem Status "open"
	 *   von der Vergangenheit bis zum Zeitpunkt der Durchführung
	 * 
	 * @return ClassRuns
	 */
	@RolesAllowed("instruktor")
	public List<ClassRun> getMyOpenClassRuns(){
		HashMap<String, Object> params = new HashMap<>();
		params.put(ClassRun.PARAM_FILTER_PERSON_ID, ps.getLoggedPerson().getId());
		params.put(ClassRun.PARAM_FILTER_BOOKED_STATE, State.open);
		params.put(ClassRun.PARAM_FILTER_START_DATE, LocalDate.now());
		return findByNamedQuery(ClassRun.QUERY_FIND_MY_CLASS_RUNS_IN_DATE_RANGE_AND_BOOKED, params);
	}
	
	@RolesAllowed({"GrpAdmin","instruktor"})
	public void addcustomerAmount(ClassRun cs) throws WrongPersonException {
		if (ctx.getCallerPrincipal().getName().equals(cs.getPerson().getEmail())) {
		entityManager.merge(cs);
		}
		else
		{
			throw new WrongPersonException("Dieser Kursdurchführung gehört nicht dieser Person");
		}
	}
	@RolesAllowed("GrpAdmin")
	public void delete(ClassRun cr) {
		cr = entityManager.find(ClassRun.class, cr.getId());
		cr.setDeleted(true);
		entityManager.merge(cr);
	}

	
}
