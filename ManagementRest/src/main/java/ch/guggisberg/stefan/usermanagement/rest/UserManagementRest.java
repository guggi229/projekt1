package ch.guggisberg.stefan.usermanagement.rest;



import javax.ejb.EJB;
import javax.ejb.EJBAccessException;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import ch.guggisberg.stefan.groupfitness.course.entities.Person;
import ch.guggisberg.stefan.groupfitness.course.entities.Rollen;
import ch.guggisberg.stefan.groupfitness.course.exceptions.NewPasswordsMisMachtesException;
import ch.guggisberg.stefan.groupfitness.course.exceptions.PasswordWrongException;
import ch.guggisberg.stefan.groupfitness.course.services.PersonService;
import ch.guggisberg.stefan.groupfitness.course.services.RollenService;
import ch.guggisberg.stefan.usermanagement.model.generell.ResponeState;
import ch.guggisberg.stefan.usermanagement.model.generell.Response;
import ch.guggisberg.stefan.usermanagement.model.requests.RequestFilter;
import ch.guggisberg.stefan.usermanagement.model.requests.RequestPasswordChangeTrxModel;
import ch.guggisberg.stefan.usermanagement.model.requests.RequestPersonDeleteTrxModel;
import ch.guggisberg.stefan.usermanagement.model.responses.ResponsePersonListe;

// Aufruf: http://localhost:8080/ManagementSysRest/rest/userManagement


@Path("userManagement")
public class UserManagementRest {
	private static Logger log = Logger.getLogger(UserManagementRest.class);
	@EJB
	private PersonService ps;
	@EJB
	private RollenService rs;

	private Response r= new Response();
	private ResponsePersonListe responseList= new ResponsePersonListe();

	/**
	 * Laden aller Personen von der DB. Dabei können Filter Optionen mit gegeben werden.
	 * 
	 * @param p
	 * @return
	 */

	@GET
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("filtered")
	public ResponsePersonListe getPersonList(@Valid RequestFilter p){
		responseList.setPerson(ps.getFilteredPersons2(p.getFilter(), p.getStartPosition(), p.getMaxRows()));
		responseList.setMessage("ok");
		responseList.setResponseState(ResponeState.OK);
		return responseList;
	}

	/**
	 * Laden aller Personen von der DB
	 * 
	 * Aufruf Testsystem: http://localhost:8080/ManagementSysRest/rest/userManagement/all
	 * 
	 * @return Liste von Personen
	 */
	@GET
	@Path("all")
	@Produces({MediaType.APPLICATION_JSON})
	public ResponsePersonListe getPersonAllList(){

		try {
			responseList.setPerson(ps.getAllPersons());
			responseList.setMessage("ok");
			responseList.setResponseState(ResponeState.OK);
			return responseList;	
		} catch (EJBAccessException e) {
			log.info("Access Error", e);
			responseList.setMessage("Access Error");
			responseList.setResponseState(ResponeState.NOK);
			return responseList;
		}
		catch (Exception e) {
			log.info("Genereller Fehler", e);
			responseList.setMessage("Hopla - Da  ist was schief gelaufen");
			responseList.setResponseState(ResponeState.NOK);
			return responseList;
		}

	}

	/**
	 * Generiert einen neuen Instruktor und gibt das Objekt zurück an das Frontend.
	 * 
	 * Aufruf Testsystem: http://localhost:8080/ManagementSysRest/rest/userManagement/user
	 * 
	 * @param p
	 * @return
	 */
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("user")
	public Person createPerson(@Valid Person p) {
		Person person = new Person();
		try {
			p.addRolle(rs.find(Rollen.class, 4L));
			person = ps.updatePerson(p);
		} 
		catch (EJBAccessException e) {
			log.info("Access Error", e);
		}
		catch (Exception e) {
			log.error(e);
		}
		return person;
	}

	/**
	 * Updatet die Person. 
	 * 
	 * Aufruf Testsystem: http://localhost:8080/ManagementSysRest/rest/userManagement/user
	 * 
	 * @param p
	 * @return Person
	 */
	@PUT
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("user")
	public Person updateUser(@Valid Person p) {
		Person person = new Person();
		try {
			person = ps.update(p);
		} catch (EJBAccessException e) {
			log.info("Access Error", e);
		}
		catch (Exception e) {
			log.error(e);
		}
		return person;
	}


	@PUT
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("password")
	public Response changePassword(@Valid RequestPasswordChangeTrxModel pTrx) {
		try {
			ps.changePassword(pTrx.getNewPasswordRepeated(),pTrx.getNewPassword(), pTrx.getOldPasssword());
			r.setResponseState(ResponeState.OK);
			r.setMessage("Passwort geändert!");
		} catch (NewPasswordsMisMachtesException e) {
			r.setResponseState(ResponeState.NOK);
			r.setMessage("Die neuen Passwörter sind nicht identisch");
			log.info(e);
		} catch (PasswordWrongException e) {
			r.setResponseState(ResponeState.NOK);
			r.setMessage("Das aktuelle Passwort stimmt nicht");
			log.info(e);
		}
		return r;
	}

	/**
	 * Löschen der Person. 
	 * 
	 * Aufruf Testsystem: http://localhost:8080/ManagementSysRest/rest/userManagement/user
	 * 
	 * @param p
	 * @return Response
	 */
	@DELETE
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("user")
	public Response deletePerson(@Valid RequestPersonDeleteTrxModel persTrx) {
		try {
			ps.remove(persTrx.getId());
			r.setResponseState(ResponeState.OK);
			r.setMessage("Person gelöscht");
		} 
		catch (EJBAccessException e) {
			r.setResponseState(ResponeState.NOK);
			r.setMessage("Zugriff verweigert");
			log.info("Access Error", e);
		}
		catch (Exception e) {
			r.setResponseState(ResponeState.NOK);
			r.setMessage("Hopla - Da  ist was schief gelaufen");
			log.info("Fehler: ", e);
		}
		return r;
	}

}