package ch.guggisberg.stefan.usermanagement.rest;

import java.text.MessageFormat;

import javax.ejb.EJB;
import javax.ejb.EJBAccessException;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ch.guggisberg.stefan.groupfitness.course.entities.Kurs;
import ch.guggisberg.stefan.groupfitness.course.services.ClassService;
import ch.guggisberg.stefan.usermanagement.model.generell.ResponeState;
import ch.guggisberg.stefan.usermanagement.model.generell.Response;
import ch.guggisberg.stefan.usermanagement.model.requests.RequestKursDeleteTrxModel;
import ch.guggisberg.stefan.usermanagement.model.requests.RequestFilter;
import ch.guggisberg.stefan.usermanagement.model.responses.ResponseKursListe;

import org.apache.log4j.Logger;

//Aufruf: http://localhost:8080/ManagementSysRest/rest/classManagement
@Path("classManagement")
public class ClassManagementRest  {
	private static Logger log = Logger.getLogger(ClassManagementRest.class);
	@EJB
	private ClassService cs;
	private Response r= new Response();
	private ResponseKursListe responseListr = new ResponseKursListe();

	/**
	 * generiert einen neuen Kurstyp: Beispiel: Yoga, Aerobic
	 * 
	 * Aufruf Testsystem: http://localhost:8080/ManagementSysRest/rest/classManagement/classtyp
	 * 
	 * @param k
	 * @return Kurs
	 */

	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("classtyp")
	public Kurs createClass(@Valid Kurs k) {
		Kurs kurs = new Kurs();
		try {
			kurs = cs.createKurs(k);
		} 
		catch (EJBAccessException e) {
			log.info("Access Error", e);
		}
		catch (Exception e) {
			log.error(e);
		}
		return kurs;
	}

	/**
	 * Listet alle Kursarten auf: Beispiel: Yoga, Pilates
	 * 
	 * 	 Aufruf Testsystem: http://localhost:8080/ManagementSysRest/rest/classManagement/all
	 * 
	 * @param k
	 * @return Liste mit Kurs
	 */
	@GET
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("all")
	public ResponseKursListe getKursList(@Valid RequestFilter k){
		responseListr.setKurse(cs.getAllClasses());
		responseListr.setMessage("ok");
		responseListr.setResponseState(ResponeState.OK);
		return responseListr;
	}
	
	/**
	 * 
	 * Markiert den Kurstyp als gelöscht.
	 * Keine physikalische Löschung findet statt.
	 * 	 
	 * 
	 * Aufruf Testsystem: http://localhost:8080/ManagementSysRest/rest/classManagement/classtyp
	 * 
	 * 
	 * @param k
	 * @return
	 */
	@DELETE
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("classtyp")
	public Response remove(@Valid RequestKursDeleteTrxModel k) {
		cs.remove(k.getId());
		r.setResponseState(ResponeState.OK);
		r.setMessage(MessageFormat.format("Kurs {0} gelöscht" , cs.getKurs(k.getId()).getNameDe()));
		return r;
	}
	
	/**
	 * Updatet den Kurs. 
	 * 
	 * Aufruf Testsystem: http://localhost:8080/ManagementSysRest/rest/classManagement/classtyp
	 * 
	 * @param k
	 * @return Kurs
	 */
	@PUT
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@Path("classtyp")
	public Kurs updateClass(@Valid Kurs k) {
		Kurs kurs = new Kurs();
		try {
			kurs = cs.updateKurs(k);
		} catch (EJBAccessException e) {
			log.info("Access Error", e);
		}
		catch (Exception e) {
			log.error(e);
		}
		return kurs;
	}

}
