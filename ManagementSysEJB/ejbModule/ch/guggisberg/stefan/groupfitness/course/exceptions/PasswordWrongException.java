package ch.guggisberg.stefan.groupfitness.course.exceptions;

/**
 * Wird geworfen, wenn zum eingeloggten User das aktuelle Passwort nicht stimmt.
 * @author guggi229
 *
 */
public class PasswordWrongException extends Exception {

	private static final long serialVersionUID = -3539560965880545074L;

	public PasswordWrongException(String errorMessage) {
		super(errorMessage);
	}
}
