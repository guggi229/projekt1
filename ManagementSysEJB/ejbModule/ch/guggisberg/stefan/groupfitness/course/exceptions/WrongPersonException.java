package ch.guggisberg.stefan.groupfitness.course.exceptions;

public class WrongPersonException extends Exception{
	private static final long serialVersionUID = 3560632554554629532L;

	public WrongPersonException(String errorMessage) {
		super(errorMessage);
	}
}

