package ch.guggisberg.stefan.groupfitness.course.services;

import java.util.HashMap;
import java.util.List;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import ch.guggisberg.stefan.groupfitness.course.entities.MonthlyPayroll;
import ch.guggisberg.stefan.groupfitness.course.model.State;

@Stateless
@LocalBean
@DeclareRoles({"GrpAdmin", "customer", "Admin", "instruktor","admin"})
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class MonthlyPayrollService extends BaseCrud<MonthlyPayroll>  {

	@RolesAllowed("instruktor")
	public MonthlyPayroll createMonthlyPayRoll(MonthlyPayroll m) {
		entityManager.persist(m);
		return m;
	}
	
	@RolesAllowed({"GrpAdmin", "finance"})
	public List<MonthlyPayroll> getVerifyList(State state) {
		HashMap<String, Object> params = new HashMap<>();
		params.put(MonthlyPayroll.PARAM_STATE, state);
		return findByNamedQuery(MonthlyPayroll.QUERY_MONTHLY_PAYROLL,params);
	}
		
	@RolesAllowed({"GrpAdmin","instruktor", "finance"})
	public MonthlyPayroll getMonthlyPayroll(Long id) {
		return entityManager.find(MonthlyPayroll.class, id);
	}
	
	@RolesAllowed("GrpAdmin")
	@Override
	public MonthlyPayroll find(Class<MonthlyPayroll> type, Object id) {
		return super.find(type, id);
	}
	
	@RolesAllowed({"GrpAdmin", "finance"})
	@Override
	public MonthlyPayroll update(MonthlyPayroll mp) {
		return super.update(mp);
	}
	

}
