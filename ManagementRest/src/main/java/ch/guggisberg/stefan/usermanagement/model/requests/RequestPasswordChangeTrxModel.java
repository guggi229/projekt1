package ch.guggisberg.stefan.usermanagement.model.requests;

import java.io.Serializable;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class RequestPasswordChangeTrxModel implements Serializable {

	private static final long serialVersionUID = -4097447663122951546L;
	
	@Pattern(regexp = "^[A-Za-z0-9äÄöÖüÜ]+$")
	@Size( max=45)
	private String oldPasssword;
	
	@Pattern(regexp = "^[A-Za-z0-9äÄöÖüÜ]+$")
	@Size( max=45)
	private String newPassword;
	
	@Pattern(regexp = "^[A-Za-z0-9äÄöÖüÜ]+$")
	@Size( max=45)
	private String newPasswordRepeated;
	
	public String getOldPasssword() {
		return oldPasssword;
	}
	public void setOldPasssword(String oldPasssword) {
		this.oldPasssword = oldPasssword;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getNewPasswordRepeated() {
		return newPasswordRepeated;
	}
	public void setNewPasswordRepeated(String newPasswordRepeated) {
		this.newPasswordRepeated = newPasswordRepeated;
	}


}
