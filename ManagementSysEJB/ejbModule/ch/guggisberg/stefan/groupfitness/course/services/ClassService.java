package ch.guggisberg.stefan.groupfitness.course.services;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;

import ch.guggisberg.stefan.groupfitness.course.entities.Kurs;
import ch.guggisberg.stefan.groupfitness.course.entities.Person;

@Stateless
@LocalBean
@DeclareRoles({"GrpAdmin", "customer", "Admin", "instruktor","admin"})
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ClassService extends BaseCrud<Kurs> {
	private static Logger log = Logger.getLogger(ClassService.class);

	@RolesAllowed("GrpAdmin")
	public Kurs createKurs(Kurs k) {
		entityManager.persist(k);
		return k;
	}

	@RolesAllowed("GrpAdmin")
	public Kurs updateKurs(Kurs k) {
		return entityManager.merge(k);
	}

	@RolesAllowed("GrpAdmin")
	public void remove(Long id) {
		Kurs k = getKurs(id);
		k.setDeleted(true);
		entityManager.merge(k);
		log.info(MessageFormat.format("Kurs: {0} gelöscht", k.getDescDe()));
	}

	@RolesAllowed("GrpAdmin")
	public Kurs getKurs(Long id) {
		return entityManager.find(Kurs.class, id);
	}


	@RolesAllowed("GrpAdmin")
	public List<Kurs> getFilteredClasses(String filter){
		filter= (filter.equals("") ? "%%" : MessageFormat.format("%{0}%", filter));
		filter=filter.toUpperCase();
		log.info(MessageFormat.format("Filter: {0}", filter));
		HashMap<String, Object> params = new HashMap<>();
		params.put(Kurs.PARAM_FILTER, filter);
		return findByNamedQuery(Kurs.QUERY_FIND_FILTERED_CLASSESS, params);

	}

	@RolesAllowed("GrpAdmin")
	public List<Kurs> getFilteredClasses2(String filter, Integer startPosition, Integer maxRows){
		filter= (filter.equals("") ? "%%" : MessageFormat.format("%{0}%", filter));
		filter=filter.toUpperCase();
		log.info(MessageFormat.format("Filter: {0}", filter));
		HashMap<String, Object> params = new HashMap<>();
		params.put(Kurs.PARAM_FILTER, filter);
		return findByNamedQuery(Kurs.QUERY_FIND_FILTERED_CLASSESS, params, startPosition, maxRows);

	}
	
	@RolesAllowed("GrpAdmin")
	@SuppressWarnings("unchecked")
	public List<Kurs> getAllClasses(){
		return entityManager.createNamedQuery(Kurs.QUERY_FIND_ALL_KURS).getResultList();
	}

	@RolesAllowed("GrpAdmin")
	@Override
	public Kurs find(Class<Kurs> type, Object id) {
		return super.find(type, id);
	}
	@PermitAll
	public Kurs getKursById(Long id) {
		return find(Kurs.class, id);
	}
	



}
