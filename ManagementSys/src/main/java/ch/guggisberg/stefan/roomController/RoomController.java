package ch.guggisberg.stefan.roomController;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import ch.guggisberg.stefan.base.BaseBean;
import ch.guggisberg.stefan.groupfitness.course.entities.Room;
import ch.guggisberg.stefan.groupfitness.course.services.RoomService;

@Named
@SessionScoped
public class RoomController extends BaseBean implements Serializable{

	private static final long serialVersionUID = -2626759258838436056L;

	private Room room = new Room();
	
	@EJB
	private RoomService rs;

	

	//NAV

	private static final String NAVIGATE_TO_ROOM_LIST = "/grpAdmin/roomViews/roomList";
	
	
	public void createRoom() {
		showGlobalMessage("messages.room.created");
		rs.createKurs(room);
		room=null;
	}
	public List<Room> getAllRooms(){
		return rs.getAllRooms();
	}

	public String deleteView(Room r) {
		rs.remove(r.getId());
		showGlobalMessage("messages.room.deleted");
		room=null;
		return NAVIGATE_TO_ROOM_LIST;
	}
	
	public String editView(Room r) {
		this.room = rs.find(Room.class, r.getId());
		room=null;
		return "/grpAdmin/roomViews/roomEdit";
	}

	public String update() {
		room= rs.update(room);
		showGlobalMessage("messages.room.mod");
		room=null;
		return NAVIGATE_TO_ROOM_LIST;
	}


	// Getter / Setter

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}
}
