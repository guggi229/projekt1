package ch.guggisberg.stefan.usermanagement.model.responses;

import java.io.Serializable;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import ch.guggisberg.stefan.usermanagement.model.generell.ResponeState;

public class ResponseUserAuthentication implements Serializable{

	private static final long serialVersionUID = 7773476035100088516L;
	private boolean isAuthenticated;
	
	private ResponeState responseState;
	@Pattern(regexp = "^[A-Za-z0-9äÄöÖüÜ\\s.,!/]+$")
	@Size( max=45)
	private String message;
	
	public boolean isAuthenticated() {
		return isAuthenticated;
	}
	public void setAuthenticated(boolean isAuthenticated) {
		this.isAuthenticated = isAuthenticated;
	}
	public ResponeState getResponseState() {
		return responseState;
	}
	public void setResponseState(ResponeState responseState) {
		this.responseState = responseState;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
