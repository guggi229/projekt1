package ch.guggisberg.stefan.groupfitness.course.entities;

import java.io.Serializable;
import java.util.Locale;

import javax.faces.context.FacesContext;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ch.guggisberg.stefan.groupfitness.course.model.SupprtedLanguages;

@Entity
@Table(name="CLASS")
@NamedQueries({
	@NamedQuery(name= Kurs.QUERY_FIND_FILTERED_CLASSESS, 
			query ="Select k FROM Kurs k WHERE  k.deleted=false "
					+ "AND (upper(k.nameDe) like :"  +  Kurs.PARAM_FILTER + " "
					+ "OR upper(k.nameFr) like :"  +  Kurs.PARAM_FILTER  + " "
					+ "OR upper(k.descDe) like :"  +  Kurs.PARAM_FILTER  + " "
					+ "OR upper(k.descFr) like :"  +  Kurs.PARAM_FILTER + ")"),
	@NamedQuery(name= Kurs.QUERY_FIND_ALL_KURS, 
	query ="Select k FROM Kurs k WHERE  k.deleted=false")
})


public class Kurs implements Serializable{
	private static final long serialVersionUID = -8267261828376681915L;

	public static final String QUERY_FIND_FILTERED_CLASSESS="QUERY_FIND_FILTERED_CLASSESS";
	public static final String QUERY_FIND_ALL_KURS="QUERY_FIND_ALL_KURS";
	
	public static final String PARAM_FILTER= "PARAM_FILTER";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private Long id;

	@Size(min=1, max=45)
	@Pattern(regexp = "^[A-Za-z0-9\\s]+$")
	@Column(name="NAME_DE")
	
	private String nameDe;

	@Size(min=1, max=45)
	@Pattern(regexp = "^[A-Za-z0-9\\s]+$")
	@Column(name="NAME_FR")
	
	private String nameFr;

	@JsonIgnore
	@Column(name="DESC_DE")
	@Size(max=45)
	@Pattern(regexp = "^[A-Za-z0-9\\s]+$")
	private String descDe;

	@JsonIgnore
	@Column(name="DESC_FR")
	@Pattern(regexp = "^[A-Za-z0-9\\s]+$")
	@Size(max=45)
	private String descFr;

	@Column(name="DELETED",columnDefinition="tinyint(1)")
	private boolean deleted;

	@Transient
	@JsonIgnore
	private String desc;
	
	@Transient
	@JsonIgnore
	private String name;
	

	// Getter Setter
	// =============


	public String getDesc() {
		Locale l = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		if (l==null || l.getLanguage().equals(new Locale(SupprtedLanguages.fr.toString()).getLanguage() )) return descDe;
		return descFr;
	}


	public String getName() {
		Locale l = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		if (l==null || l.getLanguage().equals(new Locale(SupprtedLanguages.de.toString()).getLanguage() )) return nameDe;
		return nameFr;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNameDe() {
		return nameDe;
	}

	public void setNameDe(String nameDe) {
		this.nameDe = nameDe;
	}

	public String getNameFr() {
		return nameFr;
	}

	public void setNameFr(String nameFr) {
		this.nameFr = nameFr;
	}

	public String getDescDe() {
		return descDe;
	}

	public void setDescDe(String descDe) {
		this.descDe = descDe;
	}

	public String getDescFr() {
		return descFr;
	}

	public void setDescFr(String descFr) {
		this.descFr = descFr;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}


}
