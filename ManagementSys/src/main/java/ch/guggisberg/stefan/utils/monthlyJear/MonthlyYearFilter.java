package ch.guggisberg.stefan.utils.monthlyJear;

import java.time.LocalDate;
import java.time.Year;

public interface MonthlyYearFilter {

	default LocalDate getFirstDay(String stringSelectedYear, int selectedMonth) {
		LocalDate selectedDate = LocalDate.of(Year.parse(stringSelectedYear).getValue(), selectedMonth, 1);
		return selectedDate.withDayOfMonth(1);
	}

	default LocalDate getLastDay(String stringSelectedYear, int selectedMonth) {
		LocalDate selectedDate = LocalDate.of(Year.parse(stringSelectedYear).getValue(), selectedMonth, 1);

		return selectedDate.withDayOfMonth(selectedDate.lengthOfMonth());
	}
	

}
