package ch.guggisberg.stefan.groupfitness.course.exceptions;

public class CrudServiceException extends Exception  {
	private static final long serialVersionUID = 906397379933810695L;

	public CrudServiceException(String errorMessage) {
		super(errorMessage);
	}
}
