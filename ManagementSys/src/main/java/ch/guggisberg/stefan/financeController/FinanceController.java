package ch.guggisberg.stefan.financeController;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import ch.guggisberg.stefan.base.BaseBean;
import ch.guggisberg.stefan.groupfitness.course.entities.ClassRun;
import ch.guggisberg.stefan.groupfitness.course.entities.MonthlyPayroll;
import ch.guggisberg.stefan.groupfitness.course.model.State;
import ch.guggisberg.stefan.groupfitness.course.services.MonthlyPayrollService;
import ch.guggisberg.stefan.groupfitness.course.services.PersonService;

@Named
@SessionScoped
public class FinanceController extends BaseBean implements Serializable {

	private static final long serialVersionUID = 174999736345493347L;

	@EJB
	private MonthlyPayrollService mps;
	
	@EJB
	private PersonService ps;
	
	private MonthlyPayroll monthlyPayroll;
	private List<ClassRun> classRuns;
	
	//NAV
	
	private static final String NAVIGATE_TO_MONTHLY_PAYROLL_DETAIL_VIEW = "/finance/payrollViews/monthlyPayrollDetailView";
	private static final String NAVIGATE_TO_FINANCE_VIEW = "/finance/financeView";
			
	public String verifyDetailView (MonthlyPayroll monthlyPayroll) {
		this.monthlyPayroll = mps.getMonthlyPayroll(monthlyPayroll.getId());
		this.classRuns=monthlyPayroll.getClassRuns();
		return NAVIGATE_TO_MONTHLY_PAYROLL_DETAIL_VIEW;
	}
	
	public String reject() {
		monthlyPayroll.setState(State.rejected);
		mps.update(monthlyPayroll);
		showGlobalMessage(("messages.finance.payroll.rejeacted"));
		return NAVIGATE_TO_FINANCE_VIEW;
	}
	
	public String book() {
		monthlyPayroll.setState(State.ready2export);
		mps.update(monthlyPayroll);
		showGlobalMessage(("messages.finance.ready2export"));
		return NAVIGATE_TO_FINANCE_VIEW;
	}
	

// Getter / Setter

	
	public List<ClassRun> getClasses(){
		return classRuns;
	}
	
	public List<MonthlyPayroll> getPayRollList(){
		return mps.getVerifyList(State.approved);
	}


	public MonthlyPayroll getMonthlyPayroll() {
		return monthlyPayroll;
	}


	public void setMonthlyPayroll(MonthlyPayroll monthlyPayroll) {
		this.monthlyPayroll = monthlyPayroll;
	}
	
	
}
