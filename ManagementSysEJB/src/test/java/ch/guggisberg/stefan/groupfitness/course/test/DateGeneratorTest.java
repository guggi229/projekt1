package ch.guggisberg.stefan.groupfitness.course.test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.Month;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import ch.guggisberg.stefan.groupfitness.course.exceptions.DayOfWeekOutOfRangeException;
import ch.guggisberg.stefan.groupfitness.course.exceptions.StartDateHigherThanEndDateException;
import ch.guggisberg.stefan.groupfitness.course.utils.DateGenerator;

public class DateGeneratorTest {

	private DateGenerator dg;
	private LocalDate startDate;
	private LocalDate endDate;

	
	
	@Test
	public void test() throws StartDateHigherThanEndDateException, DayOfWeekOutOfRangeException {
		startDate =  LocalDate.of(2018, Month.JANUARY, 1);
		endDate = LocalDate.of(2018, Month.JANUARY, 21);
		dg = new DateGenerator();
		Set<LocalDate> dateList= new HashSet<LocalDate>(); 
		dateList = dg.getAllDays(startDate, endDate, 1);
		
		assertTrue(dateList.contains(LocalDate.of(2018, Month.JANUARY, 1)));
		assertTrue(dateList.contains(LocalDate.of(2018, Month.JANUARY, 8)));
		assertTrue(dateList.contains(LocalDate.of(2018, Month.JANUARY, 15)));
	
	}
	
	@Test(expected = DayOfWeekOutOfRangeException.class)
	public void dayOutOfRange() throws StartDateHigherThanEndDateException, DayOfWeekOutOfRangeException{
		startDate =  LocalDate.of(2018, Month.JANUARY, 1);
		endDate = LocalDate.of(2018, Month.JANUARY, 21);
		dg = new DateGenerator();
		@SuppressWarnings("unused")
		Set<LocalDate> dateList= new HashSet<LocalDate>(); 
		dateList = dg.getAllDays(startDate, endDate, 8);
	}
	
	@Test(expected = StartDateHigherThanEndDateException.class)
	public void StartDateHigherThanEndDateTest() throws StartDateHigherThanEndDateException, DayOfWeekOutOfRangeException{
		startDate =  LocalDate.of(2018, Month.JANUARY, 21);
		endDate = LocalDate.of(2018, Month.JANUARY, 1);
		dg = new DateGenerator();
		@SuppressWarnings("unused")
		Set<LocalDate> dateList= new HashSet<LocalDate>(); 
		dateList = dg.getAllDays(startDate, endDate, 2);
	}

}
