package ch.guggisberg.stefan.financeController;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import org.apache.log4j.Logger;

import ch.guggisberg.stefan.base.BaseBean;
import ch.guggisberg.stefan.groupfitness.course.entities.MonthlyPayroll;
import ch.guggisberg.stefan.groupfitness.course.model.State;
import ch.guggisberg.stefan.groupfitness.course.services.MonthlyPayrollService;
import ch.guggisberg.stefan.utils.file.ERPFileGenerator;

@Named
@RequestScoped
public class ERPExporterController  extends BaseBean implements Serializable {

	private static final long serialVersionUID = -7423034487445810161L;

	private static Logger log = Logger.getLogger(ERPExporterController.class);
	private ERPFileGenerator erpFileGenerator;

	@EJB
	private MonthlyPayrollService mps;

	private List<MonthlyPayroll> monthlyPayroll;

	
	//NAV
	
	private static final String NAVIGATE_TO_ERP_EXPORTER_VIEW = "/finance/exportViews/ERPExporterView";
	
	// Transaction!
	public String export() {
		erpFileGenerator = new ERPFileGenerator(monthlyPayroll);
		try {
			for (MonthlyPayroll payItem : monthlyPayroll) {
				payItem.setState(State.exported);
				mps.update(payItem);
			}
			erpFileGenerator.generateTheExportFile();
				showGlobalMessage(("messages.finance.download.started"));
		} catch (IOException e) {
			log.error(e);
			showGlobalErrorMessage("error.messages.hopla");
		}
		return NAVIGATE_TO_ERP_EXPORTER_VIEW;
	}

	/**
	 * Liefert die Stundenblätter mit dem Status exporting.
	 * Ready to export!
	 * 
	 * @return List<MonthlyPayroll> - zu exportieren
	 */
	public List<MonthlyPayroll> getPayRollList(){
		monthlyPayroll = mps.getVerifyList(State.ready2export);
		return monthlyPayroll;
	}

	public List<MonthlyPayroll> getMonthlyPayroll() {
		return monthlyPayroll;
	}

	public void setMonthlyPayroll(List<MonthlyPayroll> monthlyPayroll) {
		this.monthlyPayroll = monthlyPayroll;
	}



}
